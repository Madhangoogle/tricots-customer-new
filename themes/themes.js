import theme from 'styled-theming'

const Themes = {
	backgroundMain: theme('mode', {
		light: '#efefef',
		dark: '#ffffff'
	}),
	primary: theme('mode', {
		light: '#800000',
		dark: '#FFFFFF'
	}),
	white: theme('mode', {
		light: '#FFFFFF',
		dark: '#FFFFFF'
	}),
	cancelButton: theme('mode', {
		light: '#D1D1D1',
		dark: '#000'
	}),
	inputBackground: theme('mode', {
		light: '#f0f0f0',
		dark: '#000'
	}),
	negative: theme('mode', {
		light: '#EF233C',
		dark: '#000'
	}),
	positive: theme('mode', {
		light: '#7ad581',
		dark: '#000'
	})
}

export default Themes
// EF233C
// 399E5A
