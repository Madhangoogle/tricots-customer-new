const Images = {
    google: require('./images/google.jpg'),
    logo: require('./images/logo.jpg'),
    banner1: require('./images/banner_2.jpg'),
    banner2: require('./images/top-small-banner.jpg'),
    banner4: require('./images/banner-3.jpg'),
    banner5: require('./images/banner-4.jpg'),
    banner3: require('./images/banner-5.jpg'),
    collection1: require('./images/collection1.jpg'),
    collection2: require('./images/collection2.jpg'),
    collection3: require('./images/collection3.jpg'),
    collection4: require('./images/collection4.jpg'),
    collection5: require('./images/collection5.jpg'),
    collection6: require('./images/collection6.jpg'),
    category1: require('./images/category1.jpg'),
    category2: require('./images/category.jpg'),
    category3: require('./images/category3.jpg'),
    category4: require('./images/category4.jpg'),
    category5: require('./images/category5.jpg'),
    category6: require('./images/category6.jpg')
}

export default Images
