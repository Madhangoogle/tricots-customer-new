const withImages = require('next-images')
const withSass = require('@zeit/next-sass')
const withLess = require('@zeit/next-less')
const withCSS = require('@zeit/next-css')
const compose = require('next-compose')
const imagesConfig = {}
const cssConfig = {}

module.exports = compose([
  { distDir: 'build' },
  {async headers() {
    return [
      {
        // matching all API routes
        source: "/api/:path*",
        headers: [
          { key: "Access-Control-Allow-Credentials", value: "true" },
          { key: "Access-Control-Allow-Origin", value: "*" },
          { key: "Access-Control-Allow-Methods", value: "GET,OPTIONS,PATCH,DELETE,POST,PUT" },
          { key: "Access-Control-Allow-Headers", value: "X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version" },
        ]
      }
    ]
  }},

  withCSS({
    cssModules: true,
    cssLoaderOptions: {
      importLoaders: 1,
      localIdentName: '[local]___[hash:base64:5]'
    },
    ...withLess(
      withSass({
        lessLoaderOptions: {
          javascriptEnabled: true
        }
      })
    )
  }),
  [withImages, imagesConfig],
  {
    webpack: (config) => {
      config.node = {
        fs: 'empty',
        net: 'empty',
        tls: 'empty',
      }
      return config
    }
  }

])
