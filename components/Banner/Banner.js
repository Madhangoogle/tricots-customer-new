import BannerWrapper from "./Banner.style";
import { Images } from "./assets/index";
import React, { useState } from "react";
import { Carousel, Grid } from "antd";
const { useBreakpoint } = Grid;
import {useRouter} from "next/router"

export function BannerTop(props) {
	const screens = useBreakpoint();
	const { data } = props;
	const router = useRouter()
	return (
		<BannerWrapper view={screens.xs ? "mobile" : "web"}>
			{data.length !== 0 && (
				<img
					onClick={()=> router.push(data[0].link)}
					src={
						screens.xs
							? data[0].mobileImageId.imageUrl
							: data[0].imageId.imageUrl
					}
					className="banner-1"
				/>
			)}
		</BannerWrapper>
	);
}

export function BannerBottom(props) {
	const screens = useBreakpoint();
	const { data } = props;
	const router = useRouter()

	return (
		<BannerWrapper view={screens.xs ? "mobile" : "web"}>
			<Carousel autoplay>
				{data?.map((banner) => {
					return (
						<div>
							<img
								onClick={()=> router.push(banner.link)}
								src={
									screens.xs
										? banner.mobileImageId.imageUrl
										: banner.imageId.imageUrl
								}
								className="banner-2"
							/>
						</div>
					);
				})}
			</Carousel>
		</BannerWrapper>
	);
}

export default BannerBottom;
