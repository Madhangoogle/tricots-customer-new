import BannerWrapper from './BreadCrumb.style'
import React, { useState } from 'react'
import { Breadcrumb } from 'antd'

export function BreadCrumbComponent (props) {
	const { data } = props
	return (
		<BannerWrapper >
			<Breadcrumb>
				{data?.map((page) => {
					return (
						<Breadcrumb.Item>
							{page.route ? <a href={page.route}>{page.name}</a> : page.name}
						</Breadcrumb.Item>
					)
				})}
			</Breadcrumb>
		</BannerWrapper>
	)
}

export default BreadCrumbComponent
