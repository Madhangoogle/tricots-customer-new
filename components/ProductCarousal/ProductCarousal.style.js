import styled from "styled-components";
import Theme from "../../themes/themes";

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	/* margin: 5px; */
	/* width : 100vw; */
	width: 100%;
	.product-view {
	}

	.img-zoom-lens {
		position: absolute;
		border: 1px solid #d4d4d4;
		/*set the size of the lens:*/
		width: 40px;
		height: 40px;
	}
	.img-zoom-container {
		position: relative;
	}
	.img-zoom-result {
		border: 1px solid #d4d4d4;
		/*set the size of the result div:*/
		width: 300px;
		height: 300px;
	}
	.ant-collapse {
		background-color: #800000;
	}
	.ant-collapse > .ant-collapse-item > .ant-collapse-header {
		color: #ffffff;
	}
	.badge {
		position: absolute;
		text-align: left;
		background: #c32148;
		float: left;
		/* line-height: 18px; */
		color: white;
		margin-top: 0px;
		font-size: 12px;
		padding-left: 5px;
		text-transform: uppercase;
	}
	.favorite {
		position: absolute;
		background: #ffffff;
		padding: 5px;
		width: 30px;
		right: 10px;
		height: 30px;
		margin: 15px;
		display: flex;
		float: right;
		justify-content: center;
		align-items: center;
		border-radius: 50px;
		font-size: 16px;
	}
	.favoriteIcon {
		color: ${Theme.primary};
	}

	.fullImage {
		width: 100%;
		height: 100%;
		object-fit: contain;
	}
	.smallImage {
		width: 100%;
		height: 180px;
		object-fit: cover;
		cursor: pointer;
	}
	.product-image {
		width: 100%;
		object-fit: cover;
	}
	.product-name {
		width: 100%;
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
		font-size: 16px;
		font-weight: 600;
		margin-top: 20px;
	}
	.product-name2 {
		width: 100%;
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
		font-size: 20px;
		font-weight: 600;
		margin-top: 30px;
		margin-bottom: 30px;
	}
	.product-price {
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
		display: flex;
		flex-direction: row;
	}
	.colorCircle {
		width: 35px;
		height: 35px;
		border-radius: 50px;
		cursor: pointer;
	}
	.selectedColorCircle {
		width: 35px;
		height: 35px;
		border-radius: 50px;
		cursor: pointer;
		border: 3px solid #000;
	}
	.sizeBox {
		padding: 5px;
		width: 35px;
		height: 25px;
		background-color: #fff;
		border: 1px solid #000;
		cursor: pointer;
		justify-content: center;
		display: flex;
		align-items: center;
	}
	.selectedSizeBox {
		border: 3px solid #000;
		cursor: pointer;
		padding: 5px;
		width: 35px;
		height: 25px;
		background-color: #fff;
		justify-content: center;
		display: flex;
		align-items: center;
	}
	.cartText {
		font-size: 40px;
		color: ${Theme.primary};
	}
	.offer-text {
		color: ${Theme.positive};
		font-size: 12px;
	}
	.carousal-container {
		margin-top: 10px;
	}
	.sub-text {
		color: ${Theme.black};
		font-size: 12px;
	}
	.original-price-text {
		margin-right: 10px;
		color: ${Theme.negative};
		text-decoration: line-through;
	}
	.special-price-text {
		margin-right: 10px;
		color: ${Theme.primary};
		font-weight: 600;
	}
	.product-common-data {
		background-color: ${Theme.backgroundMain};
		padding: 10px;
		margin-top: 15px;
		width: 100%;
	}
	.quality-card {
		border: 1px solid ${Theme.primary};
		display: flex;
		justify-content: center;
		align-items: center;
		margin-top: 20px;
		flex-direction: column;
	}
	.color-circle {
		width: 35px;
		height: 35px;
		background-color: color;
		border-radius: 50px;
	}
	.size-box {
		padding: 5;
		width: 40px;
		height: 25px;
		background-color: #fff;
		border: 1px solid 000;
		justify-content: center;
		display: flex;
		align-items: center;
	}
`;

export default Wrapper;
