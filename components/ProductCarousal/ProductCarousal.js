import ProductCarousalWrapper from "./ProductCarousal.style";
import React, { useEffect, useState } from "react";
import { Images } from "./assets";
import { Row, Col, Grid, Button, Modal, Select, message, Input } from "antd";
import {
	HeartOutlined,
	HeartFilled,
	PlusCircleFilled,
	MinusCircleFilled,
	PlusSquareFilled,
	MinusSquareFilled,
	ShoppingCartOutlined,
	EyeOutlined,
	PaperClipOutlined,
	ShopFilled,
	ArrowRightOutlined,
	TagFilled,
} from "@ant-design/icons";
import axios from "axios";
import { API_URL } from "../../constants";
import { useRouter } from "next/router";
import { Collapse } from "antd";
import { useSelector } from "react-redux";
import { Tabs } from 'antd';

const { TabPane } = Tabs;
const { useBreakpoint } = Grid;
const { Option } = Select;

const { Panel } = Collapse;
const text = "sdcsd";
export function ProductCarousal(props) {
	const { comboCart } = useSelector((state) => state.comboCart);
	const router = useRouter();
	const {
		data,
		onClick,
		onClickPlus,
		onClickMinus,
		cart: cartD,
		onClickViewCart,
		addToWishList,
		removeFromWishList,
	} = props;
	const { cart } = cartD;
	const screens = useBreakpoint();
	const [colors, setColors] = useState([]);
	const [sizes, setSizes] = useState([]);
	const [pictures, setPictures] = useState([]);
	const [selectedColor, setSelectedColor] = useState("");
	const [selectedInventory, setSelectedInventory] = useState("");
	const [selectedPicture, setSelectedPicture] = useState("");
	const [quantity, setQuantity] = useState(1);
	const [showPopup, setPopup] = useState(false);
	// const [charts, setCharts] = useState([]);
	const [promocodes, setPromocodes] = useState();
	const [admin, setAdmin] = useState();
	const [stockCount, setStockCount] = useState([]);
	let token = localStorage.getItem("accessToken");

	const callback = () => {};
	// const getChart = () => {
	// 	axios
	// 		.get(API_URL + "/guest/charts", {
	// 			headers: {
	// 				Authorization: `JWT ${localStorage.getItem("accessToken")}`,
	// 			},
	// 		})
	// 		.then((res) => {
	// 			setCharts(res.data.data);
	// 		})
	// 		.catch((err) => {
	// 			alert(err.message);
	// 		});
	// };

	const handleLoadData = ()=> {
		if (data?.inventories) {
			let images = [];
			data?.inventories?.map((i) => {
				return (
					!images.includes(i?.imageId?.imageUrl) &&
					i?.imageId?.imageUrl !== undefined &&
					i?.imageId?.imageUrl !== null &&
					images.push(i?.imageId?.imageUrl)
				);
			});
			if (
				data?.additionalImages !== null &&
				data?.additionalImages !== undefined
			) {
				images.push(...data?.additionalImages);
			}
			setPictures(images);

			let sizeList = [];
			data?.inventories?.map((i) => {
				return !sizeList.includes(i.size) && sizeList.push(i.size);
			});
			setSizes(sizeList);
			let colorList = [];

			data?.inventories?.map((i) => {
				return !colorList.includes(i.color) && colorList.push(i.color);
			});
			setColors(colorList);
			setSelectedColor(colorList ? colorList[0] : "");
			handleSelectInventory(
				colorList ? colorList[0] : "",
				sizeList ? "S" : "S"
			);
			setSelectedPicture(data?.imageId?.imageUrl);
		}
		// getChart();
		getPromocodes();
		getAdmin();
	}
	useEffect(() => {
		handleLoadData()
	}, [data?.inventories]);

	useEffect(() => {
		if (selectedInventory) {
			if (selectedInventory && cart && cart[selectedInventory._id]) {
				setQuantity(cart && cart[selectedInventory._id].quantity);
			} else {
				setQuantity(1);
			}
		}
	}, [cartD, selectedInventory]);

	const handleSelectInventory = (color, size) => {
		var inData = data?.inventories?.filter((item) => {
			if (item.color === color && item.size === size) return item;
		});
		if (inData.length != 0) {
			setSelectedInventory(inData[0]);
		}
	};

	const getPromocodes = () => {
		axios
			.get(`${API_URL}/guest/promocodes`, {
				headers: {
					Authorization: `JWT ${localStorage.getItem("accessToken")}`,
				},
			})
			.then((res) => {
				setPromocodes(res.data.data);
			});
	};
	const getAdmin = () => {
		axios
			.get(`${API_URL}/guest/admins`, {
				headers: {
					Authorization: `JWT ${localStorage.getItem("accessToken")}`,
				},
			})
			.then((res) => {
				setAdmin(res.data.data);
			});
	};

	useEffect(() => {
		console.log(promocodes, admin);
	}, [promocodes, admin]);

	let getSizeComponent = () => {
		let available = sizes.map((size) => {
			var condition = data?.inventories?.filter((item) => {
				if (item.color === selectedColor) return item;
			});
			if (condition.length !== 0) return size;
		});

		let component = ["S", "M", "L", "XL", "XXL"].map((size) => {
			return (
				available.includes(size) && (
					<Col>
						<div
							onClick={() =>
								handleSelectInventory(selectedColor, size)
							}
							className={
								selectedInventory.size === size
									? "selectedSizeBox"
									: "sizeBox"
							}
						>
							{size}
						</div>
					</Col>
				)
			);
		});
		return component;
	};

	let getStock = () => {
		let value = selectedInventory?.stock;
		let values = [];
		for (var i = 1; i <= value; i++) {
			values.push(i);
		}
		setStockCount(values);
	};

	useEffect(() => {
		getStock();
	}, [selectedInventory]);


	const handleSubmitFeedback =()=>{

		const isLoggedIn = localStorage.getItem("accessToken")
		if(!isLoggedIn){
			return message.warn("Please, login to submit feedback")
		}
		axios
			.post(`${API_URL}/customer/feedbacks`, {
				content : review
			},{
				headers: {
					Authorization: `JWT ${localStorage.getItem("accessToken")}`,
				},
			})
			.then((res) => {
				message.success("Review posted successfully")
				props.hanldeRefresh()
			});
	}

	const [review , setReview] =useState("")
	return (
		<ProductCarousalWrapper>
			<Modal
				title="Basic Modal"
				visible={showPopup}
				onOk={() => setPopup(false)}
				onCancel={() => setPopup(false)}
			>
				<img
					src={data?.chartId?.imageId?.imageUrl}
					style={{ width: "100%", height: "100%" }}
				/>

				{data?.chartId.type === "pant" ? (
					<Row
						gutter={[10, 24]}
						className="carousal-container"
						justify="start"
					>
						<Col span={6}>Size</Col>
						<Col span={6}>In Seam Length</Col>
						<Col span={6}>Total Length</Col>
						<Col span={6}>Waist</Col>
					</Row>
				) : (
					<Row
						gutter={[10, 24]}
						className="carousal-container"
						justify="start"
					>
						<Col span={6}>Size</Col>
						<Col span={6}>Chest</Col>
						<Col span={6}>Length</Col>
						<Col span={6}>Sleeve Length</Col>
					</Row>
				)}
				{data?.chartId?.details?.map((row) => {
					if (data?.chartId.type === "pant") {
						return (
							<Row
								gutter={[10, 24]}
								className="carousal-container"
								justify="start"
							>
								<Col span={6}>{row.size}</Col>
								<Col span={6}>{row.inseamLength}</Col>
								<Col span={6}>{row.totalLength}</Col>
								<Col span={6}>{row.waist}</Col>
							</Row>
						);
					} else
						return (
							<Row
								gutter={[10, 24]}
								className="carousal-container"
								justify="start"
							>
								<Col span={6}>{row.size}</Col>
								<Col span={6}>{row.chest}</Col>
								<Col span={6}>{row.length}</Col>
								<Col span={6}>{row.sleeveLength}</Col>
							</Row>
						);
				})}
			</Modal>
			<Row
				gutter={[12, 12]}
				className={screens.xs ? "product-view" : ""}
				justify="space-between"
			>
				<Col xs={24} lg={12}>
					{/* <Row>
						<Col lg={8}>
							{[...pictures, ...data?.additionalImages, data?.imageId?.imageUrl]?.map((p) => {
								return (
									<img
										onClick={() => setSelectedPicture(p)}
										src={p}
										style={{
											width: "50%",
											objectFit: "contain",
											padding: 5,
										}}
										// className="smallImage"
									/>
								);
							})}
						</Col>
						<Col lg={16}>
							<div
								style={{
									position: "absolute",
									display: "flex",
									width: "100%",
									justifyContent: "space-between",
									padding: 5,
									zIndex: 1000,
								}}
							>
								{data?.isNewArrival && <div className="badge">New Arrival</div>}
								{data?.isFavourite ? (
									<div className="favorite">
										<HeartFilled
											className="favoriteIcon"
											onClick={() => {
												if (token) removeFromWishList(data._id);
												else message.warning("Please login to make a Wishlist");
											}}
										/>
									</div>
								) : (
									<div className="favorite">
										<HeartOutlined
											onClick={() => {
												if (token) addToWishList(data._id);
												else message.warning("Please login to make a Wishlist");
											}}
										/>
									</div>
								)}
							</div>
							{!screens.xs && (
								<img
									onClick={onClick}
									src={selectedPicture ? selectedPicture : Images.product1}
									style={{
										width: "100%",
										height: "500px",
										objectFit: "contain",
										padding: 5,
									}}
								/>
							)}
						</Col>
					</Row> */}
					<div
						style={{
							position: "absolute",
							display: "flex",
							width: "100%",
							justifyContent: "space-between",
							padding: 5,
							zIndex: 0,
						}}
					>
						{data?.isNewArrival ? (
							<div className="badge">New Arrival</div>
						) : data?.isBestSale ? (
							<div className="badge">Best Selling</div>
						) : data?.isFeatured ? (
							<div className="badge">Featured</div>
						) : null}
						{data?.isFavourite ? (
							<div className="favorite">
								<HeartFilled
									className="favoriteIcon"
									onClick={() => {
										if (token) removeFromWishList(data._id);
										else
											message.warning(
												"Please login to make a Wishlist"
											);
									}}
								/>
							</div>
						) : (
							<div className="favorite">
								<HeartOutlined
									onClick={() => {
										if (token) addToWishList(data._id);
										else
											message.warning(
												"Please login to make a Wishlist"
											);
									}}
								/>
							</div>
						)}
					</div>
					{!screens.xs && (
						<img
							onClick={onClick}
							src={
								selectedPicture
									? selectedPicture
									: Images.product1
							}
							style={{
								width: "100%",
								height: "600px",
								objectFit: "cover",
								padding: 5,
							}}
						/>
					)}
					{screens.xs && (
						<img
							src={
								selectedPicture
									? selectedPicture
									: Images.product1
							}
							style={{
								width: "100%",
								height: 300,
								objectFit: "contain",
							}}
						/>
					)}
					<Row
						gutter={[1, 10]}
						className="carousal-container"
						justify="flex-start"
						style={{ width: "100%" }}
					>
						{screens.xs ? (
							<div
								style={{
									overflow: "auto",
									whiteSpace: "nowrap",
								}}
							>
								{[...pictures, data?.imageId?.imageUrl]?.map(
									(p) => {
										return (
											<img
												onClick={() =>
													setSelectedPicture(p)
												}
												src={p}
												style={{
													padding: 5,
													width: 100,
													height: 100,
													objectFit: "contain",
												}}
												// className="smallImage1"
											/>
										);
									}
								)}
							</div>
						) : (
							[...pictures, data?.imageId?.imageUrl]?.map((p) => {
								return (
									<Col span={8}>
										<img
											onClick={() =>
												setSelectedPicture(p)
											}
											src={p}
											style={{
												padding: 5,
												width: "100%",
												height: "100%",
												objectFit: "contain",
											}}
											// className="smallImage"
										/>
									</Col>
								);
							})
						)}
					</Row>
				</Col>
				<Col xs={24} lg={12}>
					<Row gutter={0}>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">{data?.name}</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-price">
								<span className="special-price-text">
									₹{data?.specialPrice}
								</span>
								<span className="original-price-text">
									₹{data?.defaultPrice}
								</span>
							</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="offer-text">
								({data?.offerValue}% off)
							</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="sub-text">
								Inclusive Of All Taxes +{" "}
								<span className="special-price-text">
									{/* {JSON.parse(localStorage.getItem("admin"))?.deliveryCharge
										? "₹ " +
										  JSON.parse(localStorage.getItem("admin")).deliveryCharge
										: "Free Shipping"} */}
									Free Shipping
								</span>
							</div>
						</Col>
						<Col span={24}>
							<Collapse
								onChange={callback}
								style={{ marginTop: 20 }}
							>
								<Panel header="Promocodes" key="1">
									{promocodes?.map((code) => {
										return (
											<div
												style={{
													display: "flex",
													flexDirection: "row",
												}}
											>
												<TagFilled
													style={{ margin: 5 }}
												/>{" "}
												{code?.name}
											</div>
										);
									})}
								</Panel>
							</Collapse>
						</Col>
					</Row>

					<Row gutter={12}>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">Color</div>
						</Col>
						{colors.map((color) => {
							return (
								<Col>
									<div
										onClick={() => setSelectedColor(color)}
										className={
											color === selectedColor
												? "selectedColorCircle"
												: "colorCircle"
										}
										style={{
											backgroundColor: color,
										}}
									></div>
								</Col>
							);
						})}
					</Row>
					<Row gutter={12}>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							{selectedColor && (
								<div className="product-name">Size</div>
							)}
						</Col>
						{/* {sizes.map((size) => {
							var condition = data?.inventories?.filter((item) => {
								if (item.color === selectedColor) return item;
							});
							if (condition.length !== 0)
								return (
									<Col>
										<div
											onClick={() => handleSelectInventory(selectedColor, size)}
											className={
												selectedInventory.size === size
													? "selectedSizeBox"
													: "sizeBox"
											}
										>
											{size}
										</div>
									</Col>
								);
						})} */}
						{getSizeComponent()}
					</Row>
					<Row
						gutter={[12, 12]}
						justify={screens.xs ? "space-between" : "start"}
					>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">
								Quantity
								<span style={{ color: "#800000" }}>
									{selectedInventory?.stock <= 0
										? "  (OUT OF STOCK)  "
										: ""}
								</span>
							</div>
						</Col>
						<Col lg={24}>
							<Select
								disabled={selectedInventory?.stock <= 0}
								defaultValue={quantity}
								value={quantity}
								style={{ width: 150 }}
								onChange={(value) => setQuantity(value)}
							>
								{(selectedInventory?.stock <= 10
									? stockCount
									: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
								).map((q) => {
									return <Option value={q}>{q}</Option>;
								})}
							</Select>
						</Col>
						<Col >
							<Button
								disabled={selectedInventory?.stock <= 0}
								icon={<ShoppingCartOutlined />}
								type="primary"
								onClick={() =>
									onClickPlus(selectedInventory, quantity)
								}
							>
								Add To Cart
							</Button>
						</Col>
						{/* {!screens.xs && (
							<Col>
								<Button
									icon={<ShoppingCartOutlined />}
									type={"primary"}
									onClick={onClickViewCart}
								>
									View Cart
								</Button>
							</Col>
						)} */}
						<Col
							xs={screens.xs && 24}
							style={{ marginTop: screens.xs ? "15px" : "0px" }}
						>
							<Button
								style={{ width: "100%" }}
								type=""
								onClick={() => setPopup(true)}
							>
								Size Chart
							</Button>
						</Col>
						<Col
							xs={screens.xs && 24}
							style={{ marginTop: screens.xs ? "15px" : "0px" }}
						>
							<Button
								style={{ width: "100%" }}
								type="primary"
								onClick={() =>{
									onClickPlus(selectedInventory, quantity)
									router.push("/checkout")
								}
								}
							>
								Buy Now
							</Button>
						</Col>
						<Col
							xs={screens.xs && 24}
							style={{ marginTop: screens.xs ? "15px" : "0px" }}
						>
							<Button
								style={{ width: "100%" }}
								type="primary"
								onClick={() =>router.push("/checkout")}
							>
								Go to Checkout
							</Button>
						</Col>
					</Row>
					<Row gutter={12} justify="space-between">
						{/* <Col xs={12} sm={12} md={12} l={12} xl={12}>
							{selectedInventory ? (
								<Button
									onClick={() => {
										onClickPlus(selectedInventory);
									}}
									type="primary"
									style={{ marginTop: 20 }}
								>
									Add To Cart
								</Button>
							) : (
								<Row gutter={0} align="middle">
									<Col style={{ marginTop: 20 }}>
										<MinusSquareFilled
											className="cartText"
											onClick={() =>
												onClickMinus(selectedInventory)
											}
										/>
									</Col>
									<Col style={{ marginTop: 20 }}>
										<div className="cartText">
											{quantity}
										</div>
									</Col>
									<Col style={{ marginTop: 20 }}>
										<PlusSquareFilled
											className="cartText"
											onClick={() =>
												onClickPlus(selectedInventory)
											}
										/>
									</Col>
								</Row>
							)}
						</Col>
						<Col
							style={{ marginTop: 20 }}
							xs={12}
							sm={12}
							md={12}
							l={12}
							xl={12}
						>
							<Button type="" onClick={onClickViewCart}>
								View Cart
							</Button>
						</Col> */}
					</Row>
					{/* <Col xs={12} sm={12} md={24} l={24} xl={24}>
						<Button type="" onClick={() => setPopup(true)}>
							size chart
						</Button>
					</Col> */}
					<Row gutter={0} className="product-common-data">
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<li>Cash on Delivery Available</li>
							<li>Vocal for Local -100% Swadeshi Product</li>
							<li>Packed with safety</li>
						</Col>
					</Row>

					<Row gutter={0} className="product-common-data">
					<Tabs defaultActiveKey="1" onChange={callback}  style={{width : "100%"}} >
						<TabPane tab="Description" key="1"  style={{width : "100%"}} >
						<div
							dangerouslySetInnerHTML={{
								__html: data?.description,
							}}
						></div>
						</TabPane>
						<TabPane tab="Reviews" key="2">
						<Input style={{width : "100%"}} placeholder="Enter Your Feedback" onChange={(e) => setReview(e.target.value)}/>
						<Button style={{width : "100%",marginTop : 10 }} type="primary" title="" onClick={()=>{
							handleSubmitFeedback()
						}}>Submit</Button>
						</TabPane>
					</Tabs>
					
						{/* <Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">Style</div>
						</Col>
						<Col>
							<div>
								Regular Fit: Fits just right - not too tight,
								not too loose.
							</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">Fabric</div>
						</Col>
						<Col>
							<div>100% Cotton Single Jersey and bio washed.</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">Design Brief</div>
						</Col>
						<Col>
							<div>
								The savior of Gotham City is now slaying the
								attire with an amazing appearance. Wear the
								t-shirt as a cape and rule the fashion with the
								batman t-shirt.
							</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">
								Product Description
							</div>
						</Col>
						<Col>
							<div>
								“It’s not who I am underneath, but what I do
								that defines me” these are the lines by batman.
								Batman is a DC comic character popular for its
								bat-like look. Batarang sign got popular after
								the movie Batman because the sign was marked on
								the suit of batman. It has a very dark and deep
								theme, which separates it from other logos. If
								you are a true Batman fan, order this Batarang
								half sleeve t-shirt for men and have a clear
								night vision like a bat.
							</div>
						</Col>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<div className="product-name">
								Delivery & Return Policy
							</div>
						</Col>
						<Col>
							<div>
								Pay online & get free shipping. Cash Collection
								Charges applicable. Please, refer FAQ for more
								information. All products are applicable for
								return. Customers can return their order within
								15 days of the order delivery. Refunds for
								returned products will be given in your
								Respective Account.
							</div>
						</Col> */}
					</Row>
				</Col>
			</Row>
			{/* {screens.xs &&
			(Object.keys(cart).length !== 0 ||
				Object.keys(comboCart).length !== 0) ? (
				<Button
					type={"primary"}
					icon={<EyeOutlined />}
					className="add-to-cart-btn"
					onClick={() => router.push("/cart")}
				>
					View Cart (
					{Object.keys(cart).length + Object.keys(comboCart).length})
				</Button>
			) : null} */}

			<div className="product-name2">SIMILIAR PRODUCTS</div>
			{/* <Row gutter={12} justify="center" align="middle">
				<Col xs={8} sm={8} md={8} l={8} xl={8}>
					<div className="quality-card">
						<img
							src={Images.product4}
							style={{ width: "100%", objectFit: "cover" }}
						/>
						<p className="sub-text">Home Grown Brand</p>
					</div>
				</Col>
				<Col xs={8} sm={8} md={8} l={8} xl={8}>
					<div className="quality-card">
						<img
							src={Images.product4}
							style={{ width: "100%", objectFit: "cover" }}
						/>
						<p className="sub-text">100% Quality Assured</p>
					</div>
				</Col>
				<Col xs={8} sm={8} md={8} l={8} xl={8}>
					<div className="quality-card">
						<img
							src={Images.product4}
							style={{ width: "100%", objectFit: "cover" }}
						/>
						<p className="sub-text">Secure Payments</p>
					</div>
				</Col>
			</Row> */}
		</ProductCarousalWrapper>
	);
}

export default ProductCarousal;
