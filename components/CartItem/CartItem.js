import CartItemWrapper from "./CartItem.style";
import React, { useEffect, useState } from "react";
import { Images } from "./assets";
import { Row, Col, message } from "antd";
import {
	CloseCircleOutlined,
	PlusSquareFilled,
	MinusSquareFilled,
} from "@ant-design/icons";

export function CartItem(props) {
	const {
		data,
		onClickPlus,
		onClickMinus,
		cart: cartD,
		onClickCross,
		comboPage,
		onClickName,
	} = props;
	const { cart } = cartD;
	useEffect(() => {
		if (cart && cart[data._id]) {
			setQuantity(cart && cart[data._id].quantity);
		} else {
			setQuantity(1);
		}
	}, [cartD, data]);

	const [quantity, setQuantity] = useState(1);

	return (
		<CartItemWrapper>
			<Row
				gutter={0}
				align="middle"
				style={{
					width: "100%",
					borderRadius: "5px",
					boxShadow:
						"0 2px 2px 0 rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.19)",
					padding: "5px",
				}}
			>
				<Col xs={7} sm={7} md={7} l={7} xl={7}>
					<img
						onClick={() => onClickName(data?.slug)}
						src={data?.productImage ? data?.productImage : Images.product1}
						style={{
							width: "80%",
							objectFit: "cover",
							borderRadius: "5px",
						}}
					/>
				</Col>
				<Col
					xs={comboPage ? 15 : 9}
					sm={comboPage ? 15 : 9}
					md={comboPage ? 15 : 9}
					l={comboPage ? 15 : 9}
					xl={comboPage ? 15 : 9}
				>
					<div className="product-name">
						<span
							className="special-price-text"
							onClick={() => onClickName(data?.slug)}
						>
							{data?.productName}
						</span>
					</div>
					{!comboPage && (
						<div className="product-price">
							<span className="special-price-text">
								₹
								{data?.specialPrice *
									(cart && cart[data._id]?.quantity
										? cart[data._id]?.quantity
										: 1)}
							</span>
							<span className="original-price-text">
								₹
								{data?.defaultPrice *
									(cart && cart[data._id]?.quantity
										? cart[data._id]?.quantity
										: 1)}
							</span>
						</div>
					)}
					<div className="product-price">
						<span className="product-name">{data?.size}</span>
						<span
							className="product-color"
							style={{ backgroundColor: data?.color }}
						></span>
					</div>
				</Col>
				{!comboPage && (
					<Col xs={6} sm={6} md={6} l={6} xl={6}>
						<Row gutter={[12, 12]} align="middle">
							<Col>
								<MinusSquareFilled
									className="cartText"
									onClick={() => {
										onClickMinus(data, quantity);
									}}
								/>
							</Col>
							<Col>
								<div className="cartText">{quantity}</div>
							</Col>
							<Col>
								<PlusSquareFilled
									className="cartText"
									onClick={() => {
										console.log(data?.stock, quantity)
										if (data?.stock > quantity) onClickPlus(data, quantity);
										else message.warn("OUT OF STOCK")
									}}
								/>
							</Col>
						</Row>
					</Col>
				)}
				<Col xs={1} sm={1} md={1} l={1} xl={1}>
					<CloseCircleOutlined onClick={() => onClickCross(data)} />
				</Col>
			</Row>
		</CartItemWrapper>
	);
}

export function ComboCartItem(props) {
	const { data, onClickCross, comboPage, onClickName } = props;

	const [inventories, setInventories] = useState({});

	useEffect(() => {
		let dummy = data.inventories;
		setInventories(dummy);
	}, []);

	return (
		<CartItemWrapper>
			<Row
				gutter={0}
				align="middle"
				style={{
					width: "100%",
					borderRadius: "5px",
					boxShadow:
						"0 2px 2px 0 rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.19)",
					padding: "5px",
				}}
			>
				<Col xs={7} sm={7} md={7} l={7} xl={7}>
					<img
						onClick={() => onClickName(data?.comboSlug)}
						src={data?.imageUrl ? data?.imageUrl : Images.product1}
						style={{
							width: "80%",
							objectFit: "cover",
							borderRadius: "5px",
						}}
					/>
				</Col>
				<Col xs={15} sm={15} md={15} l={15} xl={15}>
					<div className="product-name">
						<span
							className="special-price-text"
							onClick={() => onClickName(data?.comboSlug)}
						>
							{data?.productName}
						</span>
					</div>
					{!comboPage && (
						<div className="product-price">
							<span className="special-price-text">₹{data?.specialPrice}</span>
							<span className="original-price-text">₹{data?.defaultPrice}</span>
						</div>
					)}
					{Object.keys(inventories)?.map((inventory) => {
						
							return (
								<div
									style={{
										display: "flex",
										flexDirection: "row",
										justifyContent: "space-between",
										width: "80%",
										marginTop: 5,
									}}
								>
									<span style={{fontSize : 13}}>{inventories[inventory].size}{" "}</span>
										<div
											style={{
												width: 10,
												height: 10,
												backgroundColor: inventories[inventory].color,
												border: "1px solid #000",
												marginTop : 5
											}}
										></div>
									<span style={{fontSize : 13}}>{inventories[inventory].colorName}{`  - `}</span>
									<span  style={{fontSize : 13}}>{inventories[inventory].quantity}{" "}</span>
								</div>
							);
					})}
				</Col>

				<Col xs={1} sm={1} md={1} l={1} xl={1}>
					<CloseCircleOutlined onClick={() => onClickCross(data)} />
				</Col>
			</Row>
		</CartItemWrapper>
	);
}

export default ComboCartItem;
