import styled from "styled-components";
import Theme from "../../themes/themes";

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	position: fixed;
	z-index: 1000;
	.ant-drawer-title {
		font-family: "Montserrat", sans-serif;
	}
	/* .menu-item {
		font-family: "Montserrat", sans-serif;
		height: 20px;
		line-height: 20px;
		margin: 30px;
		background-color: red;
	} */
	.ant-menu-sub.ant-menu-inline > .ant-menu-item, .ant-menu-sub.ant-menu-inline > .ant-menu-submenu > .ant-menu-submenu-title{
		height: 20px;
	}

	/* background-color : red; */
	.track-order-container {
		width: 100%;
		height: 3vh;
		font-size : 12px;
		background-color:#800000;
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: row;
	}
	.track-order {
		width: 95%;
		display: flex;
		justify-content: space-between;
		align-items: center;
		flex-direction: row;
		.top-header-text {
			cursor: pointer;
			color: #fff;
		}
	}
	.top-header-container {
		width: 100%;
		height: 6vh;
		background-color: #fff;
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: row;
	}
	.top-header {
		width: 100%;
		display: flex;
		justify-content: space-between;
		align-items: center;
		flex-direction: row;
		padding: 10px;
		.top-header-text {
			cursor: pointer;
			color: #000;
		}
		.header-text {
			font-size: 14px;
			font-weight: 600;
			cursor: pointer;
			padding-left: 10px;
			padding-right: 10px;
			padding-top: 5px;
			padding-bottom: 5px;
			background-color: #fff;
			color: #000;
			border: none;
		}
		.header-text :hover {
			padding-left: 10px;
			padding-right: 10px;
			padding-top: 5px;
			padding-bottom: 5px;
			color: #fff;
			background-color:#800000;
			margin-top: -10px;
		}
		.header-icons {
			font-size: 20px;
		}
	}

	.header-dropdown {
		width: 300px;
		height: 300px;
		background-color: #fff;
		display: flex;
		justify-content: space-between;
		align-items: center;
	}
`;

export default Wrapper;
