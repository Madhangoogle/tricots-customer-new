import styled from 'styled-components'
import Theme from '../../themes/themes'

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	/* margin: 5px; */
	width : 100%;
	.combo-item{
		margin-top : 10px;
		margin-bottom : 10px;
	}

`

export default Wrapper
