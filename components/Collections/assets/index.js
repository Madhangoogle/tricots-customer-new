const Images = {
    collection1: require('./images/collection1.jpg'),
    collection2: require('./images/collection2.jpg'),
    collection3: require('./images/collection3.jpg'),
    collection4: require('./images/collection4.jpg'),
    collection5: require('./images/collection5.jpg'),
    collection6: require('./images/collection6.jpg')
}
export {
	Images
}
const ComponentAssets = {
	Images
}
export default ComponentAssets
