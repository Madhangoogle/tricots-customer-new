import styled from 'styled-components'
import Theme from '../../themes/themes'

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.wrapper {
		/* width  : 100%; */
		margin: 5px;
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
		.direction-icon {
			font-size: 30px;
			cursor: pointer;
		}
		.title {
			font-size: 24px;
			font-weight: 400;
			margin-top:  ${props => props.mobile ? "5px" : "20px"};
			margin-bottom:  ${props => props.mobile ? "5px" : "20px"};
			/* border : 1px solid #000;
		padding : 10px; */
		}
		.collection {
			height: ${props => props.mobile ? "100%" : "100%"};
			width: ${props => props.mobile ? "100%" : "100%"};
			object-fit: contain;
			border-radius: 5px;
			cursor: pointer;

		}

		.wrapper:hover #slide {
			transition: 1s;
			left: 0;
		}

		#slide {
			position: absolute;
			left: -100px;
			width: 100px;
			height: 100px;
			background: blue;
			-webkit-animation: slide 0.5s forwards;
			-webkit-animation-delay: 2s;
			animation: slide 0.5s forwards;
			animation-delay: 2s;
		}

		@-webkit-keyframes slide {
			100% {
				left: 0;
			}
		}

		@keyframes slide {
			100% {
				left: 0;
			}
		}
	}
`

export default Wrapper
