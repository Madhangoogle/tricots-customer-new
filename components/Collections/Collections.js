import CollectionWrapper from "./Collections.style";
import { Images } from "./assets/index";
import React, { useState } from "react";
import { Row, Col, Grid } from "antd";

import { LeftCircleFilled, RightCircleFilled } from "@ant-design/icons";
const { useBreakpoint } = Grid;

export function Collections(props) {
	const { data, onClickCollection } = props;
	const screens = useBreakpoint();
	let small = [1, 2, 7, 8, 13, 14, 19, 20, 25, 26, 30, 31];
	return (
		<CollectionWrapper mobile={screens.xs}>
			<div className="wrapper">
				<div className="title">COLLECTIONS</div>
				<Row gutter={[8, 8]} justify={"center"} align="middle">
					{/* <Col lg={3} xs={0}>
						<Row gutter={[8, 8]}>
							<Col span={24}>
								<LeftCircleFilled className="direction-icon" />
							</Col>
						</Row>
					</Col> */}
					{/* <Col lg={24} xs={24}>
						<img
							onClick={() =>
								onClickCollection(
									data[0]?.name
										.split(" ")
										.map((word) => word.toLowerCase())
										.join("-")
								)
							}
							src={data[0]?.imageId.imageUrl}
							className="collection"
						/>
					</Col> */}
					<Col lg={24} xs={24}>
						<Row gutter={[10, 10]}>
							{data?.map((collection, index) => {
								return (
									<Col
										xs={12}
										lg={small.includes(index + 1) ? 12 : 6}
										style={{ justifyContent: "center", display: "flex" }}
									>
										<span
											style={{
												cursor: "pointer",
												position: "absolute",
												backgroundColor: "#fff",
												paddingLeft: 10,
												paddingRight: 10,
												paddingBlock: 2,
												borderRadius: 5,
												fontSize: screens.xs ? 10 : 24,
												bottom: 15,
												justifyContent: "center",
												display: "flex",
											}}
										>
											{collection?.name}
										</span>
										<img
											onClick={() => onClickCollection(collection?.slug)}
											src={collection.imageId.imageUrl}
											className="collection"
										/>
									</Col>
								);
							})}
						</Row>
					</Col>
					{/* <Col lg={3} xs={0}>
						<Row gutter={[8, 8]}>
							<Col span={24}>
								<RightCircleFilled className="direction-icon" />
							</Col>
						</Row>
					</Col> */}
				</Row>
			</div>
		</CollectionWrapper>
	);
}

export default Collections;
