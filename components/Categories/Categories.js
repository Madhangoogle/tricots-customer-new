import CategoryWrapper from "./Categories.style";
import { Images } from "./assets/index";
import React, { useState } from "react";
import { Row, Col } from "antd";

import { LeftCircleFilled, RightCircleFilled } from "@ant-design/icons";

export function Categories(props) {
	const { data, onClickCollection } = props;
	return (
		<CategoryWrapper view="mobile">
			<div className="wrapper">
				<div className="title">CATEGORIES</div>
				<Row gutter={[8, 8]} align="middle">
					{/* <Col lg={3} xs={0}>
						<LeftCircleFilled className="direction-icon" />
					</Col> */}
					<Col lg={12} xs={24}>
						<img
							onClick={() =>
								onClickCollection(
									data[0]?.name
										.split(" ")
										.map((word) => word.toLowerCase())
										.join("-")
								)
							}
							src={data[0]?.imageId.imageUrl}
							className="category"
						/>
					</Col>
					<Col lg={12} xs={24}>
						<Row gutter={[10, 10]}>
							{data?.map((collection, index) => {
								return (
									index > 0 && (
										<Col span={12}>
											<img
												onClick={() =>
													onClickCollection(
														collection?.name
															.split(" ")
															.map((word) =>
																word.toLowerCase()
															)
															.join("-")
													)
												}
												src={
													collection.imageId.imageUrl
												}
												className="category"
											/>
										</Col>
									)
								);
							})}
						</Row>
					</Col>
					{/* <Col lg={3} xs={0}>
						<RightCircleFilled className="direction-icon" />
					</Col> */}
				</Row>
			</div>
		</CategoryWrapper>
	);
}

export default Categories;
