import styled from 'styled-components'
import UGTheme from '../../themes/UGTheme'

const Wrapper = styled.section`
	width: 100%;

	.card {
		width: 100%;
		padding: 20px;
		background-color: ${UGTheme.cardSecondaryGrey};
		border-radius: 5px;
		margin-top: 20px;
		.cardHeader {
			display: flex;
			align-items: flex-start;
			justify-content: flex-start;
			width: 100%;

			.cardImg {
				border-radius: 100%;
				object-fit: cover;
			}

			.cardHeaderSubText {
				width: 100%;
				color: ${UGTheme.white};
				margin-left: 10px;
				flex-direction: column;
				font-weight: 400;
				font-size: 13px;
				.nameText {
					width: 100%;
					font-size: small;
					font-weight: 500;
					display: flex;
					flex-direction: row;
					justify-content: space-between;
					color: ${(props) => (props.onHover ? UGTheme.black : UGTheme.white)};
					.removeButton {
						color: ${UGTheme.secondary};
					}
				}
				.subText {
					font-size: 13px;
					font-weight: 400;
					color: ${(props) => (props.onHover ? UGTheme.black : UGTheme.white)};
				}
				.capitalise {
					text-transform: capitalize;
				}

				.blockBtn {
					width: 100%;
					height: 30px;
					background-color: ${UGTheme.primary};
					border-radius: 3px;
					border: none;
					color: ${UGTheme.white};
				}
			}
		}
	}
`

export default Wrapper
