import LinesEllipsis from 'react-lines-ellipsis'
import { Row, Col, Button } from 'antd'

import PropTypes from 'prop-types'
import AddMemberWrapper from './AddMember.style'
import { Images } from './assets/index'

export default function ProfileHeader (props) {
	ProfileHeader.propTypes = {
		text: PropTypes.any,
		subText: PropTypes.any,
		secondaryText: PropTypes.any,
		customStyles: PropTypes.any,
		removeButton: PropTypes.any,
		onCancel: PropTypes.any,
		imageUrl: PropTypes.any,
		onClickAdd: PropTypes.any
	}

	const { text, subText, secondaryText, customStyles, removeButton, onClickAdd, imageUrl } = props
	return (
		<AddMemberWrapper>
			<div className="card">
				<div className="cardHeader" style={customStyles}>
					<img className="cardImg" alt="Avatar" width="60px" height="60px" src={imageUrl} />
					<div className="cardHeaderSubText">
						<div className="nameText">
							<LinesEllipsis text={text} maxLine="2" ellipsis="..." trimRight basedOn="letters" />
							{removeButton && (
								<div className="removeButton">
									<img width={7} style={{ marginRight: '5px' }} src={Images.close} />
									<span style={{ fontSize: '10px' }}>Remove</span>
								</div>
							)}
						</div>
						<Row gutter={16}>
							<Col span={12}>
								<div className="subText capitalise" style={{ textTransform: 'capitalize' }}>{subText.toLowerCase()}</div>
								<div className="subText">{secondaryText}</div>
							</Col>
							<Col span={12}>
								<Button className="blockBtn" type="primary" onClick={onClickAdd}>
									Add
								</Button>
							</Col>
						</Row>
					</div>
				</div>
			</div>
		</AddMemberWrapper>
	)
}
