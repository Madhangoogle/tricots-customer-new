import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	createComboCartFailure,
	createComboCartSuccess,
	addToComboCartFailure,
	addToComboCartSuccess,
	deleteComboCartFailure,
	deleteComboCartSuccess,
	removeFromComboCartFailure,
	removeFromComboCartSuccess,
	emptyComboCartFailure,
	emptyComboCartSuccess,
} from "../actions/comboCart";

export function* addToComboCart({ inventoryId, quantity }) {
	const { comboCart } = yield select((state) => state.comboCart);
	try {
		if (!comboCart[inventoryId])
			yield put(addToComboCartFailure("No ComboCart Item Found"));
		if (comboCart[inventoryId]) {
			let comboCartDetails = comboCart;
			comboCartDetails[inventoryId].quantity = quantity;
			yield put(addToComboCartSuccess(comboCartDetails));
		}
	} catch (error) {
		yield put(addToComboCartFailure(error.toString()));
	}
}

export function* removeFromComboCart({ inventoryId }) {
	const { comboCart } = yield select((state) => state.comboCart);
	try {
		if (!comboCart[inventoryId])
			yield put(removeFromComboCartFailure("No ComboCart Item Found"));
		if (comboCart[inventoryId]) {
			let comboCartDetails = comboCart;
			comboCartDetails[inventoryId].quantity =
				comboCartDetails[inventoryId].quantity - 1;
			yield put(removeFromComboCartSuccess(comboCartDetails));
		}
	} catch (error) {
		yield put(removeFromComboCartFailure(error.toString()));
	}
}

export function* createComboCart({ data }) {
	const { comboCart } = yield select((state) => state.comboCart);

	try {
		yield put(createComboCartSuccess({ ...data, ...comboCart }));
	} catch (error) {
		yield put(createComboCartFailure(error.toString()));
	}
}

export function* deleteComboCart({ inventoryId }) {
	const { comboCart } = yield select((state) => state.comboCart);
	try {
		if (!comboCart[inventoryId])
			yield put(deleteComboCartFailure("No ComboCart Item Found"));
		if (comboCart[inventoryId]) {
			let comboCartDetails = comboCart;
			delete comboCartDetails[inventoryId];
			yield put(deleteComboCartSuccess(comboCartDetails));
		}
	} catch (error) {
		yield put(deleteComboCartFailure(error.toString()));
	}
}
export function* emptyComboCart() {
	try {
		yield put(emptyComboCartSuccess({}));
	} catch (error) {
		yield put(emptyComboCartFailure(error.toString()));
	}
}

const ComboCartSaga = {
	addToComboCart,
	removeFromComboCart,
	deleteComboCart,
	createComboCart,
	emptyComboCart
};

export default ComboCartSaga;
