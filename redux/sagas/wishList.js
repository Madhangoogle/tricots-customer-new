import { call, select, put } from 'redux-saga/effects'
import request from '../../util/request'

import { API_URL } from '../../constants'
import {
	getWishListSuccess,
	getWishListFailure,

	addToWishListSuccess,
	addToWishListFailure,

	removeFromWishListSuccess,
	removeFromWishListFailure,
} from '../actions/wishList'

export function * wishListGet () {
	const state = yield select()
	const accessToken = state.auth.accessToken
	try {
		const response = yield call(request, API_URL + '/customer/favourites', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
				Authorization: `JWT ${accessToken}`
			}
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(getWishListSuccess(response.data))
		} else {
			yield put(getWishListFailure(response.meta.message))
		}
	} catch (error) {
		yield put(getWishListFailure(error.toString()))
	}
}

export function * wishListAdd ({ productId }) {
	const state = yield select()
	const accessToken = state.auth.accessToken
	try {
        const response = yield call(request, API_URL + '/customer/favourites', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json;charset=utf-8',
					Authorization: `JWT ${accessToken}`
				},
				body: JSON.stringify({productId : productId})
			}
		)
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(addToWishListSuccess(response.data))
		} else {
			yield put(addToWishListFailure(response.meta.message))
		}
	} catch (error) {
		yield put(addToWishListFailure(error.toString()))
	}
}

export function * wishListRemove ({productId}) {
	const state = yield select()
	const accessToken = state.auth.accessToken
	try {
		const response = yield call(request, API_URL + '/customer/favourites/deleteProduct', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
				Authorization: `JWT ${accessToken}`
			},
			body : JSON.stringify({productId : productId})
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(removeFromWishListSuccess(response.data))
		} else {
			yield put(removeFromWishListFailure(response.meta.message))
		}
	} catch (error) {
		yield put(removeFromWishListFailure(error.toString()))
	}
}


const WishListSaga = {
    wishListGet,
    wishListAdd,
	wishListRemove,
}

export default WishListSaga
