import { call, select, put } from 'redux-saga/effects'
import request from '../../util/request'

import { API_URL } from '../../constants'
import {
	getProductsSuccess,
	getProductsFailure,
} from '../actions/products'

export function * productsGet () {
	const state = yield select()
	try {
		const response = yield call(request, API_URL + `/guest/products`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
			}
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(getProductsSuccess(response.data))
		} else {
			yield put(getProductsFailure(response.meta.message))
		}
	} catch (error) {
		yield put(getProductsFailure(error.toString()))
	}
}

const ProductsSaga = {
	productsGet,
}

export default ProductsSaga
