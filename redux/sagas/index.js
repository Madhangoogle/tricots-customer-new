import { all, takeLatest } from 'redux-saga/effects'

import AuthActions from '../actions/auth'
import CustomersActions from '../actions/customers'
import ProfileActions from '../actions/profile'
import GeneralActions from '../actions/general'
import CartActions from '../actions/cart'
import ComboCartActions from '../actions/comboCart'
import WishListActions from '../actions/wishList'

import AuthSaga from './auth'
import CustomerSaga from './customers'
import ProfileSaga from './profile'
import GeneralSaga from './general'
import CartSaga from './cart'
import ComboCartSaga from './comboCart'
import WishListSaga from './wishList'

function * rootSaga () {
	yield all([
		takeLatest(AuthActions.actionTypes.LOGIN_REQUEST, AuthSaga.loginPost),
		takeLatest(AuthActions.actionTypes.LOGOUT_REQUEST, AuthSaga.logoutPost),
		takeLatest(AuthActions.actionTypes.SIGNUP_REQUEST, AuthSaga.signupPost),

		takeLatest(CustomersActions.actionTypes.GET_CUSTOMERS_REQUEST, CustomerSaga.customersGet),
		takeLatest(CustomersActions.actionTypes.GET_CUSTOMER_DETAILS_REQUEST, CustomerSaga.customerDetailsGet),
		takeLatest(CustomersActions.actionTypes.CREATE_CUSTOMER_REQUEST, CustomerSaga.customerCreate),
		takeLatest(CustomersActions.actionTypes.UPDATE_CUSTOMER_REQUEST, CustomerSaga.customerUpdate),
		takeLatest(CustomersActions.actionTypes.DELETE_CUSTOMER_REQUEST, CustomerSaga.customerDelete),

		takeLatest(ProfileActions.actionTypes.GET_PROFILE_DETAILS_REQUEST, ProfileSaga.profileDetailsGet),
		takeLatest(ProfileActions.actionTypes.UPDATE_PROFILE_DETAILS_REQUEST, ProfileSaga.profileDetailsUpdate),

		takeLatest(ProfileActions.actionTypes.GET_ADDRESSES_REQUEST, ProfileSaga.addressesGet),
		takeLatest(ProfileActions.actionTypes.GET_ADDRESS_DETAILS_REQUEST, ProfileSaga.addressDetailsGet),
		takeLatest(ProfileActions.actionTypes.CREATE_ADDRESS_REQUEST, ProfileSaga.addressCreate),
		takeLatest(ProfileActions.actionTypes.UPDATE_ADDRESS_REQUEST, ProfileSaga.addressUpdate),
		takeLatest(ProfileActions.actionTypes.DELETE_ADDRESS_REQUEST, ProfileSaga.addressDelete),

		takeLatest(GeneralActions.actionTypes.GET_BANNERS_REQUEST, GeneralSaga.bannersGet),
		takeLatest(GeneralActions.actionTypes.GET_COLLECTIONS_REQUEST, GeneralSaga.collectionsGet),
		takeLatest(GeneralActions.actionTypes.GET_PRODUCTS_REQUEST, GeneralSaga.productsGet),
		takeLatest(GeneralActions.actionTypes.GET_PRODUCT_DETAILS_REQUEST, GeneralSaga.productDetailsGet),
		takeLatest(GeneralActions.actionTypes.GET_HEADERS_REQUEST, GeneralSaga.headersGet),
		takeLatest(GeneralActions.actionTypes.GET_COMBO_REQUEST, GeneralSaga.comboGet),
		takeLatest(GeneralActions.actionTypes.GET_COMBO_DETAILS_REQUEST, GeneralSaga.comboDetailsGet),
		takeLatest(GeneralActions.actionTypes.SET_LOADER_REQUEST, GeneralSaga.loaderSet),

		takeLatest(CartActions.actionTypes.ADD_TO_CART_REQUEST, CartSaga.addToCart),
		takeLatest(CartActions.actionTypes.REMOVE_FROM_CART_REQUEST, CartSaga.removeFromCart),
		takeLatest(CartActions.actionTypes.CREATE_CART_REQUEST, CartSaga.createCart),
		takeLatest(CartActions.actionTypes.DELETE_CART_REQUEST, CartSaga.deleteCart),
		takeLatest(CartActions.actionTypes.EMPTY_CART_REQUEST, CartSaga.emptyCart),

		takeLatest(ComboCartActions.actionTypes.ADD_TO_COMBO_CART_REQUEST, ComboCartSaga.addToComboCart),
		takeLatest(ComboCartActions.actionTypes.REMOVE_FROM_COMBO_CART_REQUEST, ComboCartSaga.removeFromComboCart),
		takeLatest(ComboCartActions.actionTypes.CREATE_COMBO_CART_REQUEST, ComboCartSaga.createComboCart),
		takeLatest(ComboCartActions.actionTypes.DELETE_COMBO_CART_REQUEST, ComboCartSaga.deleteComboCart),
		takeLatest(ComboCartActions.actionTypes.EMPTY_COMBO_CART_REQUEST, ComboCartSaga.emptyComboCart),

		takeLatest(WishListActions.actionTypes.ADD_TO_WISHLIST_REQUEST, WishListSaga.wishListAdd),
		takeLatest(WishListActions.actionTypes.REMOVE_FROM_WISHLIST_REQUEST, WishListSaga.wishListRemove),
		takeLatest(WishListActions.actionTypes.GET_WISHLIST_REQUEST, WishListSaga.wishListGet),
	])
}

export default rootSaga
