import { call, select, put } from 'redux-saga/effects'
import request from '../../util/request'

import { API_URL } from '../../constants'
import {
	getAddressesSuccess,
	getAddressesFailure,

	createAddressSuccess,
	createAddressFailure,

	updateAddressSuccess,
	updateAddressFailure,

	deleteAddressSuccess,
    deleteAddressFailure,

    getAddressDetailsSuccess,
    getAddressDetailsFailure,

    getProfileDetailsSuccess,
    getProfileDetailsFailure,

    updateProfileDetailsSuccess,
    updateProfileDetailsFailure
} from '../actions/profile'

export function * profileDetailsGet () {
	const state = yield select()
	const accessToken = state.auth.accessToken
	try {
		const response = yield call(request, API_URL + '/customer/profiles', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
				Authorization: `JWT ${accessToken}`
			}
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(getProfileDetailsSuccess(response.data))
		} else {
			yield put(getProfileDetailsFailure(response.meta.message))
		}
	} catch (error) {
		yield put(getProfileDetailsFailure(error.toString()))
	}
}

export function * profileDetailsUpdate ({ data }) {
	const state = yield select()
	const accessToken = state.auth.accessToken
	try {
        const response = yield call(request, API_URL + '/customer/profiles', {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json;charset=utf-8',
					Authorization: `JWT ${accessToken}`
				},
				body: JSON.stringify(data)
			}
		)
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(updateProfileDetailsSuccess(response.data))
		} else {
			yield put(updateProfileDetailsFailure(response.meta.message))
		}
	} catch (error) {
		yield put(updateProfileDetailsFailure(error.toString()))
	}
}

export function * addressesGet () {
	const state = yield select()
	const accessToken = state.auth.accessToken
	try {
		const response = yield call(request, API_URL + '/customer/addresses', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
				Authorization: `JWT ${accessToken}`
			}
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(getAddressesSuccess(response.data))
		} else {
			yield put(getAddressesFailure(response.meta.message))
		}
	} catch (error) {
		yield put(getAddressesFailure(error.toString()))
	}
}

export function * addressDetailsGet ({ addressId }) {
	const state = yield select()
	const accessToken = state.auth.accessToken
	try {
		const response = yield call(request, API_URL + '/customer/addresses/' + addressId, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
				Authorization: `JWT ${accessToken}`
			}
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(getAddressDetailsSuccess(response.data))
		} else {
			yield put(getAddressDetailsFailure(response.meta.message))
		}
	} catch (error) {
		yield put(getAddressDetailsFailure(error.toString()))
	}
}

export function * addressCreate ({ data }) {
	const state = yield select()
	const accessToken = state.auth.accessToken
	try {
		const response = yield call(request, API_URL + '/customer/addresses', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
				Authorization: `JWT ${accessToken}`
			},
			body: JSON.stringify(data)
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(createAddressSuccess(response.data))
		} else {
			yield put(createAddressFailure(response.meta.message))
		}
	} catch (error) {
		yield put(createAddressFailure(error.toString()))
	}
}

export function * addressUpdate ({ addressId, data }) {
	const state = yield select()
	const accessToken = state.auth.accessToken
	try {
        const response = yield call(request, API_URL + '/customer/addresses/' + addressId, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json;charset=utf-8',
					Authorization: `JWT ${accessToken}`
				},
				body: JSON.stringify(data)
			}
		)
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(updateAddressSuccess(response.data))
		} else {
			yield put(updateAddressFailure(response.meta.message))
		}
	} catch (error) {
		yield put(updateAddressFailure(error.toString()))
	}
}

export function * addressDelete ({ addressId }) {
	const state = yield select()
	const accessToken = state.auth.accessToken
	try {
		const response = yield call(
			request,
			API_URL + '/customer/addresses/' + addressId,
			{
				method: 'DELETE',
				headers: {
					'Content-Type': 'application/json;charset=utf-8',
					Authorization: `JWT ${accessToken}`
				}
			}
		)
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(deleteAddressSuccess(response.data))
		} else {
			yield put(deleteAddressFailure(response.meta.message))
		}
	} catch (error) {
		yield put(deleteAddressFailure(error.toString()))
	}
}

const ProfileSaga = {
    profileDetailsGet,
    profileDetailsUpdate,
	addressesGet,
	addressDetailsGet,
	addressCreate,
	addressUpdate,
	addressDelete
}

export default ProfileSaga
