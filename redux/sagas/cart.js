import { call, select, put } from "redux-saga/effects";
import request from "../../util/request";

import { API_URL } from "../../constants";
import {
	createCartFailure,
	createCartSuccess,
	addToCartFailure,
	addToCartSuccess,
	deleteCartFailure,
	deleteCartSuccess,
	removeFromCartFailure,
	removeFromCartSuccess,
	emptyCartFailure,
	emptyCartSuccess
} from "../actions/cart";

export function* addToCart({ inventoryId, quantity }) {
    const {cart} = yield select(state => state.cart);
	try {
        if(!cart[inventoryId])
            yield put(addToCartFailure("No Cart Item Found"));
        if(cart[inventoryId]){
            let cartDetails = cart
            cartDetails[inventoryId].quantity = quantity
            yield put(addToCartSuccess(cartDetails));
        }
	} catch (error) {
		yield put(addToCartFailure(error.toString()));
	}
}

export function* removeFromCart({ inventoryId }) {
    const {cart} = yield select(state => state.cart);
	try {
        if(!cart[inventoryId])
            yield put(removeFromCartFailure("No Cart Item Found"));
        if(cart[inventoryId]){
            let cartDetails = cart
            cartDetails[inventoryId].quantity = cartDetails[inventoryId].quantity -1
            yield put(removeFromCartSuccess(cartDetails));
        }
	} catch (error) {
		yield put(removeFromCartFailure(error.toString()));
	}
}

export function* createCart({ data }) {
    const {cart} = yield select(state => state.cart);

	try {
            yield put(createCartSuccess({...data, ...cart}));
	} catch (error) {
		yield put(createCartFailure(error.toString()));
	}
}

export function* deleteCart({inventoryId}) {
    const {cart} = yield select(state => state.cart);
	try {
        if(!cart[inventoryId])
            yield put(deleteCartFailure("No Cart Item Found"));
        if(cart[inventoryId]){
            let cartDetails = cart
			delete cartDetails[inventoryId]
            yield put(deleteCartSuccess(cartDetails));
        }
	} catch (error) {
		yield put(deleteCartFailure(error.toString()));
	}
}

export function* emptyCart() {
	try {
		yield put( emptyCartSuccess({}))
	} catch (error) {
		yield put(emptyCartFailure(error.toString()));
	}
}

const CartSaga = {
	addToCart,
	removeFromCart,
	deleteCart,
	createCart,
	emptyCart
};

export default CartSaga;
