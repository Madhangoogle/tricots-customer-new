import { call, select, put } from 'redux-saga/effects'
import request from '../../util/request'

import { API_URL } from '../../constants'
import {
	loginSuccess,
	loginFailure,
	logoutSuccess,

	signupSuccess,
	signupFailure
} from '../actions/auth'

export function * loginPost ({ token }) {
	try {
		const response = yield call(request, API_URL + '/customer/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json;charset=utf-8'
			},
			body: JSON.stringify({
				token
			})
		})
		if (response) {
			console.log(response)
			const accessToken = response.data.jwtToken
			yield put(loginSuccess(accessToken))
		} else {
			yield put(loginFailure(response?.message ? response?.message : 'Unauthorized'))
		}
	} catch (error) {
		yield put(loginFailure('Please check your username and/or password.')) // do this only for login response
		// yield put(loginFailure(error.toString())); // in all other cases use this
	}
}

export function * logoutPost () {
	const state = yield select()
	const accessToken = state.auth.accessToken
	try {
		yield call(request, API_URL + '/customer/logout', {
			method: 'POST',
			headers: {
				Authorization: accessToken
			}
		})
		yield put(logoutSuccess())
	} catch (error) {
		yield put(logoutSuccess())
	}
}

export function * signupPost ({ data }) {
	try {
		const response = yield call(request, API_URL + '/customer/signup', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json;charset=utf-8'
			},
			body: JSON.stringify(data)
		})
		if (response && response.apiStatus === 'SUCCESS') {
			yield put(signupSuccess(response.data))
		} else {
			yield put(signupFailure(response.meta.message))
		}
	} catch (error) {
		yield put(signupFailure(error.toString()))
	}
}

const AuthSaga = {
	loginPost,
	logoutPost,
	signupPost
}

export default AuthSaga
