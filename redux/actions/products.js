export const actionTypes = {
	CLEAR_ERROR: 'PRODUCTS/CLEAR_ERROR',
	STATE_RESET: 'PRODUCTS/STATE_RESET',

	GET_PRODUCTS_REQUEST: 'PRODUCTS/GET_PRODUCTS_REQUEST',
	GET_PRODUCTS_SUCCESS: 'PRODUCTS/GET_PRODUCTS_SUCCESS',
    GET_PRODUCTS_FAILURE: 'PRODUCTS/GET_PRODUCTS_FAILURE',
}

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR })
export const stateReset = (resetState) => ({ type: actionTypes.STATE_RESET, resetState })

export const getProductsRequest = (collectionId) => ({ type: actionTypes.GET_PRODUCTS_REQUEST, collectionId  })
export const getProductsFailure = (error) => ({ type: actionTypes.GET_PRODUCTS_FAILURE, error })
export const getProductsSuccess = (products) => ({ type: actionTypes.GET_PRODUCTS_SUCCESS, products })

const ProductActions = {
	actionTypes,

	clearError,
	stateReset,

    getProductsRequest,
    getProductsFailure,
    getProductsSuccess,

}

export default ProductActions
