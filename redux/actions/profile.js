export const actionTypes = {
	CLEAR_ERROR: 'PROFILE/CLEAR_ERROR',
    STATE_RESET: 'PROFILE/STATE_RESET',

	GET_PROFILE_DETAILS_REQUEST: 'PROFILE/GET_PROFILE_DETAILS_REQUEST',
	GET_PROFILE_DETAILS_SUCCESS: 'PROFILE/GET_PROFILE_DETAILS_SUCCESS',
    GET_PROFILE_DETAILS_FAILURE: 'PROFILE/GET_PROFILE_DETAILS_FAILURE',

    UPDATE_PROFILE_DETAILS_REQUEST: 'PROFILE/UPDATE_PROFILE_DETAILS_REQUEST',
	UPDATE_PROFILE_DETAILS_SUCCESS: 'PROFILE/UPDATE_PROFILE_DETAILS_SUCCESS',
	UPDATE_PROFILE_DETAILS_FAILURE: 'PROFILE/UPDATE_PROFILE_DETIALS_FAILURE',

	GET_ADDRESSES_REQUEST: 'PROFILE/GET_ADDRESSES_REQUEST',
	GET_ADDRESSES_SUCCESS: 'PROFILE/GET_ADDRESSES_SUCCESS',
    GET_ADDRESSES_FAILURE: 'PROFILE/GET_ADDRESSES_FAILURE',

	GET_ADDRESS_DETAILS_REQUEST: 'PROFILE/GET_ADDRESS_DETAILS_REQUEST',
	GET_ADDRESS_DETAILS_SUCCESS: 'PROFILE/GET_ADDRESS_DETAILS_SUCCESS',
    GET_ADDRESS_DETAILS_FAILURE: 'PROFILE/GET_ADDRESS_DETAILS_FAILURE',

    CREATE_ADDRESS_REQUEST: 'PROFILE/CREATE_ADDRESS_REQUEST',
	CREATE_ADDRESS_SUCCESS: 'PROFILE/CREATE_ADDRESS_SUCCESS',
    CREATE_ADDRESS_FAILURE: 'PROFILE/CREATE_ADDRESS_FAILURE',

    UPDATE_ADDRESS_REQUEST: 'PROFILE/UPDATE_ADDRESS_REQUEST',
    UPDATE_ADDRESS_SUCCESS: 'PROFILE/UPDATE_ADDRESS_SUCCESS',
    UPDATE_ADDRESS_FAILURE: 'PROFILE/UPDATE_ADDRESS_FAILURE',

    DELETE_ADDRESS_REQUEST: 'PROFILE/DELETE_ADDRESS_REQUEST',
	DELETE_ADDRESS_SUCCESS: 'PROFILE/DELETE_ADDRESS_SUCCESS',
    DELETE_ADDRESS_FAILURE: 'PROFILE/DELETE_ADDRESS_FAILURE'
}

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR })
export const stateReset = (resetState) => ({ type: actionTypes.STATE_RESET, resetState })

export const getProfileDetailsRequest = () => ({ type: actionTypes.GET_PROFILE_DETAILS_REQUEST })
export const getProfileDetailsFailure = (error) => ({ type: actionTypes.GET_PROFILE_DETAILS_FAILURE, error })
export const getProfileDetailsSuccess = (profileDetails) => ({ type: actionTypes.GET_PROFILE_DETAILS_SUCCESS, profileDetails })

export const updateProfileDetailsRequest = (data) => ({ type: actionTypes.UPDATE_PROFILE_DETAILS_REQUEST, data })
export const updateProfileDetailsFailure = (error) => ({ type: actionTypes.UPDATE_PROFILE_DETAILS_FAILURE, error })
export const updateProfileDetailsSuccess = (profileDetails) => ({ type: actionTypes.UPDATE_PROFILE_DETAILS_SUCCESS, profileDetails })

export const getAddressesRequest = (addressId) => ({ type: actionTypes.GET_ADDRESSES_REQUEST, addressId })
export const getAddressesFailure = (error) => ({ type: actionTypes.GET_ADDRESSES_FAILURE, error })
export const getAddressesSuccess = (addresses) => ({ type: actionTypes.GET_ADDRESSES_SUCCESS, addresses })

export const getAddressDetailsRequest = (addressId) => ({ type: actionTypes.GET_ADDRESS_DETAILS_REQUEST, addressId })
export const getAddressDetailsFailure = (error) => ({ type: actionTypes.GET_ADDRESS_DETAILS_FAILURE, error })
export const getAddressDetailsSuccess = (addressDetails) => ({ type: actionTypes.GET_ADDRESS_DETAILS_SUCCESS, addressDetails })

export const createAddressRequest = (data) => ({ type: actionTypes.CREATE_ADDRESS_REQUEST, data })
export const createAddressFailure = (error) => ({ type: actionTypes.CREATE_ADDRESS_FAILURE, error })
export const createAddressSuccess = (addressDetails) => ({ type: actionTypes.CREATE_ADDRESS_SUCCESS, addressDetails })

export const updateAddressRequest = (addressId, data) => ({ type: actionTypes.UPDATE_ADDRESS_REQUEST, addressId, data })
export const updateAddressFailure = (error) => ({ type: actionTypes.UPDATE_ADDRESS_FAILURE, error })
export const updateAddressSuccess = (addressDetails) => ({ type: actionTypes.UPDATE_ADDRESS_SUCCESS, addressDetails })

export const deleteAddressRequest = (addressId) => ({ type: actionTypes.DELETE_ADDRESS_REQUEST, addressId })
export const deleteAddressFailure = (error) => ({ type: actionTypes.DELETE_ADDRESS_FAILURE, error })
export const deleteAddressSuccess = () => ({ type: actionTypes.DELETE_ADDRESS_SUCCESS })

const AuthActions = {
	actionTypes,

	clearError,
    stateReset,

    getProfileDetailsRequest,
    getProfileDetailsFailure,
    getAddressesSuccess,

    updateProfileDetailsRequest,
    updateProfileDetailsFailure,
    updateProfileDetailsSuccess,

    getAddressesRequest,
    getAddressesFailure,
    getAddressesSuccess,

    getAddressDetailsRequest,
    getAddressDetailsFailure,
    getAddressDetailsSuccess,

    createAddressRequest,
    createAddressFailure,
    createAddressSuccess,

    updateAddressRequest,
    updateAddressFailure,
    updateAddressSuccess,

    deleteAddressRequest,
    deleteAddressFailure,
    deleteAddressSuccess

}

export default AuthActions
