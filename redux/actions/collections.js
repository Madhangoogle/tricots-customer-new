export const actionTypes = {
	CLEAR_ERROR: 'COLLECTIONS/CLEAR_ERROR',
	STATE_RESET: 'COLLECTIONS/STATE_RESET',

	GET_COLLECTIONS_REQUEST: 'COLLECTIONS/GET_COLLECTIONS_REQUEST',
	GET_COLLECTIONS_SUCCESS: 'COLLECTIONS/GET_COLLECTIONS_SUCCESS',
    GET_COLLECTIONS_FAILURE: 'COLLECTIONS/GET_COLLECTIONS_FAILURE',
}

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR })
export const stateReset = (resetState) => ({ type: actionTypes.STATE_RESET, resetState })

export const getCollectionsRequest = () => ({ type: actionTypes.GET_COLLECTIONS_REQUEST  })
export const getCollectionsFailure = (error) => ({ type: actionTypes.GET_COLLECTIONS_FAILURE, error })
export const getCollectionsSuccess = (collections) => ({ type: actionTypes.GET_COLLECTIONS_SUCCESS, collections })

const CollectionActions = {
	actionTypes,

	clearError,
	stateReset,

    getCollectionsRequest,
    getCollectionsFailure,
    getCollectionsSuccess,

}

export default CollectionActions
