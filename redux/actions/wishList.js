export const actionTypes = {
	CLEAR_ERROR: 'WISHLIST/CLEAR_ERROR',
	STATE_RESET: 'WISHLIST/STATE_RESET',

	GET_WISHLIST_REQUEST: 'WISHLIST/GET_WISHLIST_REQUEST',
	GET_WISHLIST_SUCCESS: 'WISHLIST/GET_WISHLIST_SUCCESS',
    GET_WISHLIST_FAILURE: 'WISHLIST/GET_WISHLIST_FAILURE',

	ADD_TO_WISHLIST_REQUEST: 'WISHLIST/ADD_TO_WISHLIST_REQUEST',
	ADD_TO_WISHLIST_SUCCESS: 'WISHLIST/ADD_TO_WISHLIST_SUCCESS',
    ADD_TO_WISHLIST_FAILURE: 'WISHLIST/ADD_TO_WISHLIST_FAILURE',

	REMOVE_FROM_WISHLIST_REQUEST: 'WISHLIST/REMOVE_FROM_WISHLIST_REQUEST',
	REMOVE_FROM_WISHLIST_SUCCESS: 'WISHLIST/REMOVE_FROM_WISHLIST_SUCCESS',
    REMOVE_FROM_WISHLIST_FAILURE: 'WISHLIST/REMOVE_FROM_WISHLIST_FAILURE',
}

export const clearError = () => ({ type: actionTypes.CLEAR_ERROR })
export const stateReset = (resetState) => ({ type: actionTypes.STATE_RESET, resetState })

export const getWishListRequest = () => ({ type: actionTypes.GET_WISHLIST_REQUEST,   })
export const getWishListFailure = (error) => ({ type: actionTypes.GET_WISHLIST_FAILURE, error })
export const getWishListSuccess = (wishListProducts) => ({ type: actionTypes.GET_WISHLIST_SUCCESS, wishListProducts })

export const addToWishListRequest = (productId) => ({ type: actionTypes.ADD_TO_WISHLIST_REQUEST, productId  })
export const addToWishListFailure = (error) => ({ type: actionTypes.ADD_TO_WISHLIST_FAILURE, error })
export const addToWishListSuccess = (wishListProductDetails) => ({ type: actionTypes.ADD_TO_WISHLIST_SUCCESS, wishListProductDetails })

export const removeFromWishListRequest = (productId) => ({ type: actionTypes.REMOVE_FROM_WISHLIST_REQUEST, productId  })
export const removeFromWishListFailure = (error) => ({ type: actionTypes.REMOVE_FROM_WISHLIST_FAILURE, error })
export const removeFromWishListSuccess = (wishListProductDetails) => ({ type: actionTypes.REMOVE_FROM_WISHLIST_SUCCESS, wishListProductDetails })

const WishListActions = {
	actionTypes,

	clearError,
	stateReset,

    getWishListRequest,
    getWishListFailure,
    getWishListSuccess,

    addToWishListRequest,
    addToWishListFailure,
    addToWishListSuccess,

    removeFromWishListRequest,
    removeFromWishListFailure,
    removeFromWishListSuccess,


}

export default WishListActions
