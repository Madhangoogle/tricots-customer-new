import { actionTypes } from '../actions/cart'

export const initialState = {

    addingToCart: false,
    addToCartSuccessful: false,
    cart: {},

    removingFromCartDetails: false,
    removeFromCartSuccessful: false,

    creatingCart: false,
    createCartSuccessful: false,

    deletingCart: false,
    deleteCartSuccessful: false,

    emptyingCart : false,
    emptyCartSuccessful : false,

	error: null
}

function customerReducer (state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } }
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState }

		case actionTypes.ADD_TO_CART_REQUEST: return { ...state, ...{ addingToCart: true } }
		case actionTypes.ADD_TO_CART_FAILURE: return { ...state, ...{ addingToCart: false, error: action.error } }
		case actionTypes.ADD_TO_CART_SUCCESS: return { ...state, ...{ addingToCart: false, addToCartSuccessful: true, cart: action.cart } }

        case actionTypes.REMOVE_FROM_CART_REQUEST: return { ...state, ...{ removingFromCartDetails: true } }
		case actionTypes.REMOVE_FROM_CART_FAILURE: return { ...state, ...{ removingFromCartDetails: false, error: action.error } }
		case actionTypes.REMOVE_FROM_CART_SUCCESS: return { ...state, ...{ removingFromCartDetails: false, removeFromCartSuccessful: true, cart: action.cart } }

        case actionTypes.CREATE_CART_REQUEST: return { ...state, ...{ creatingCart: true } }
		case actionTypes.CREATE_CART_FAILURE: return { ...state, ...{ creatingCart: false, error: action.error } }
        case actionTypes.CREATE_CART_SUCCESS: return { ...state, ...{ creatingCart: false, createCartSuccessful: true, cart: action.cart } }
        
        case actionTypes.DELETE_CART_REQUEST: return { ...state, ...{ deletingCart: true } }
		case actionTypes.DELETE_CART_FAILURE: return { ...state, ...{ deletingCart: false, error: action.error } }
        case actionTypes.DELETE_CART_SUCCESS: return { ...state, ...{ deletingCart: false, deleteCartSuccessful: true, cart: action.cart } }

        case actionTypes.EMPTY_CART_REQUEST: return { ...state, ...{ emptyingCart: true } }
		case actionTypes.EMPTY_CART_FAILURE: return { ...state, ...{ emptyingCart: false, error: action.error } }
        case actionTypes.EMPTY_CART_SUCCESS: return { ...state, ...{ emptyingCart: false, emptyCartSuccessful: true, cart:action.cart } }

		default:return state
	}
}

export default customerReducer
