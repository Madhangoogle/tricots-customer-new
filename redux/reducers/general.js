import { actionTypes } from '../actions/general'

export const initialState = {

    fetchingBanners: false,
    fetchBannersSuccessful: false,
    banners: [],

    fetchingCollections: false,
    fetchCollectionsSuccessful: false,
    collections: [],

    fetchingProducts: false,
    fetchProductsSuccessful: false,
    products: [],

    fetchingCombo: false,
    fetchComboSuccessful: false,
    combo: [],

    fetchingComboDetails: false,
    fetchComboDetailsSuccessful: false,
    comboDetails: null,

    fetchingHeaders: false,
    fetchHeadersSuccessful: false,
    headers: [],

    fetchingProductDetails: false,
    fetchProductDetailsSuccessful: false,
    productDetails: null,

    settingLoader: false,
    setLoaderSuccessful: false,
    loading: false,

	error: null
}

function generalReducer (state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } }
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState }

		case actionTypes.GET_BANNERS_REQUEST: return { ...state, ...{ fetchingBanners: true } }
		case actionTypes.GET_BANNERS_FAILURE: return { ...state, ...{ fetchingBanners: false, error: action.error } }
        case actionTypes.GET_BANNERS_SUCCESS: return { ...state, ...{ fetchingBanners: false, fetchBannersSuccessful: true, banners: action.banners } }
        
        case actionTypes.GET_COLLECTIONS_REQUEST: return { ...state, ...{ fetchingCollections: true } }
		case actionTypes.GET_COLLECTIONS_FAILURE: return { ...state, ...{ fetchingCollections: false, error: action.error } }
        case actionTypes.GET_COLLECTIONS_SUCCESS: return { ...state, ...{ fetchingCollections: false, fetchCollectionSuccessful: true, collections: action.collections } }
        
		case actionTypes.GET_PRODUCTS_REQUEST: return { ...state, ...{ fetchingProducts: true } }
		case actionTypes.GET_PRODUCTS_FAILURE: return { ...state, ...{ fetchingProducts: false, error: action.error } }
		case actionTypes.GET_PRODUCTS_SUCCESS: return { ...state, ...{ fetchingProducts: false, fetchProductsSuccessful: true, products: action.products } }
       
		case actionTypes.GET_COMBO_REQUEST: return { ...state, ...{ fetchingCombo: true } }
		case actionTypes.GET_COMBO_FAILURE: return { ...state, ...{ fetchingCombo: false, error: action.error } }
		case actionTypes.GET_COMBO_SUCCESS: return { ...state, ...{ fetchingCombo: false, fetchComboSuccessful: true, combo: action.combo } }
       
		case actionTypes.GET_COMBO_DETAILS_REQUEST: return { ...state, ...{ fetchingComboDetails: true } }
		case actionTypes.GET_COMBO_DETAILS_FAILURE: return { ...state, ...{ fetchingComboDetails: false, error: action.error } }
		case actionTypes.GET_COMBO_DETAILS_SUCCESS: return { ...state, ...{ fetchingComboDetails: false, fetchComboDetailsSuccessful: true, comboDetails: action.comboDetails } }

        case actionTypes.GET_PRODUCT_DETAILS_REQUEST: return { ...state, ...{ fetchingProductDetails: true } }
		case actionTypes.GET_PRODUCT_DETAILS_FAILURE: return { ...state, ...{ fetchingProductDetails: false, error: action.error } }
		case actionTypes.GET_PRODUCT_DETAILS_SUCCESS: return { ...state, ...{ fetchingProductDetails: false, fetchProductDetailsSuccessful: true, productDetails: action.productDetails } }

        case actionTypes.GET_HEADERS_REQUEST: return { ...state, ...{ fetchingHeaders: true } }
		case actionTypes.GET_HEADERS_FAILURE: return { ...state, ...{ fetchingHeaders: false, error: action.error } }
		case actionTypes.GET_HEADERS_SUCCESS: return { ...state, ...{ fetchingHeaders: false, fetchHeadersSuccessful: true, headers: action.headers } }

        case actionTypes.SET_LOADER_REQUEST: return { ...state, ...{ settingLoader: true } }
		case actionTypes.SET_LOADER_FAILURE: return { ...state, ...{ settingLoader: false, error: action.error } }
		case actionTypes.SET_LOADER_SUCCESS: return { ...state, ...{ settingLoader: false, setLoaderSuccessful: true, loading: action.loading } }

		default:return state
	}
}

export default generalReducer
