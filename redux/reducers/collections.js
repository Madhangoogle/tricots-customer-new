import { actionTypes } from '../actions/collections'

export const initialState = {

    fetchingCollections: false,
    fetchCollectionsSuccessful: false,
    collections: [],

	error: null
}

function collectionsReducer (state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } }
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState }

		case actionTypes.GET_COLLECTIONS_REQUEST: return { ...state, ...{ fetchingCollections: true } }
		case actionTypes.GET_COLLECTIONS_FAILURE: return { ...state, ...{ fetchingCollections: false, error: action.error } }
		case actionTypes.GET_COLLECTIONS_SUCCESS: return { ...state, ...{ fetchingCollections: false, fetchCollectionSuccessful: true, collections: action.collections } }

		default:return state
	}
}

export default collectionsReducer
