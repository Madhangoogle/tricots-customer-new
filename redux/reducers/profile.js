import { actionTypes } from '../actions/profile'

export const initialState = {

    fetchingAddresses: false,
    fetchAddressesSuccessful: false,
    addresses: [],

    fetchingAddressDetails: false,
    fetchAddressDetailsSuccessful: false,
    addressDetails: null,

    creatingAddress: false,
    createAddressSuccessful: false,

    updatingAddress: false,
    updateAddressSuccessful: false,

    deletingAddress: false,
    deleteAddressSuccessful: false,

    updatingProfileDetails: false,
    updateProfileDetailsSuccessful: false,

    fetchingProfileDetails: false,
    fetchProfileDetailsSuccessful: false,
    profileDetails: null,

	error: null
}

function profileReducer (state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } }
        case actionTypes.STATE_RESET: return { ...state, ...action.resetState }

        case actionTypes.GET_PROFILE_DETAILS_REQUEST: return { ...state, ...{ fetchingProfileDetails: true } }
		case actionTypes.GET_PROFILE_DETAILS_FAILURE: return { ...state, ...{ fetchingProfileDetails: false, error: action.error } }
		case actionTypes.GET_PROFILE_DETAILS_SUCCESS: return { ...state, ...{ fetchingProfileDetails: false, fetchProfileDetailsSuccessful: true, profileDetails: action.profileDetails } }

        case actionTypes.UPDATE_PROFILE_DETAILS_REQUEST: return { ...state, ...{ updatingProfileDetails: true } }
		case actionTypes.UPDATE_PROFILE_DETAILS_FAILURE: return { ...state, ...{ updatingProfileDetails: false, error: action.error } }
        case actionTypes.UPDATE_PROFILE_DETAILS_SUCCESS: return { ...state, ...{ updatingProfileDetails: false, updateProfileDetailsSuccessful: true, profileDetails: action.profileDetails } }

		case actionTypes.GET_ADDRESSES_REQUEST: return { ...state, ...{ fetchingAddresses: true } }
		case actionTypes.GET_ADDRESSES_FAILURE: return { ...state, ...{ fetchingAddresses: false, error: action.error } }
		case actionTypes.GET_ADDRESSES_SUCCESS: return { ...state, ...{ fetchingAddresses: false, fetchAddressesSuccessful: true, addresses: action.addresses } }

        case actionTypes.GET_ADDRESS_DETAILS_REQUEST: return { ...state, ...{ fetchingAddressDetails: true } }
		case actionTypes.GET_ADDRESS_DETAILS_FAILURE: return { ...state, ...{ fetchingAddressDetails: false, error: action.error } }
		case actionTypes.GET_ADDRESS_DETAILS_SUCCESS: return { ...state, ...{ fetchingAddressDetails: false, fetchAddressDetailsSuccessful: true, addressDetails: action.addressDetails } }

        case actionTypes.CREATE_ADDRESS_REQUEST: return { ...state, ...{ creatingAddress: true } }
		case actionTypes.CREATE_ADDRESS_FAILURE: return { ...state, ...{ creatingAddress: false, error: action.error } }
		case actionTypes.CREATE_ADDRESS_SUCCESS: return { ...state, ...{ creatingAddress: false, createAddressSuccessful: true, addressDetails: action.addressDetails } }

        case actionTypes.UPDATE_ADDRESS_REQUEST: return { ...state, ...{ updatingAddress: true } }
		case actionTypes.UPDATE_ADDRESS_FAILURE: return { ...state, ...{ updatingAddress: false, error: action.error } }
        case actionTypes.UPDATE_ADDRESS_SUCCESS: return { ...state, ...{ updatingAddress: false, updateAddressSuccessful: true, addressDetails: action.addressDetails } }

        case actionTypes.DELETE_ADDRESS_REQUEST: return { ...state, ...{ deletingAddress: true } }
		case actionTypes.DELETE_ADDRESS_FAILURE: return { ...state, ...{ deletingAddress: false, error: action.error } }
        case actionTypes.DELETE_ADDRESS_SUCCESS: return { ...state, ...{ deletingAddress: false, deleteAddressSuccessful: true, addressDetails: null } }

		default:return state
	}
}

export default profileReducer
