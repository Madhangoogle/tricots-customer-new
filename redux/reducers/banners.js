import { actionTypes } from '../actions/banners'

export const initialState = {

    fetchingBanners: false,
    fetchBannersSuccessful: false,
    banners: [],

	error: null
}

function bannersReducer (state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } }
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState }

		case actionTypes.GET_BANNERS_REQUEST: return { ...state, ...{ fetchingBanners: true } }
		case actionTypes.GET_BANNERS_FAILURE: return { ...state, ...{ fetchingBanners: false, error: action.error } }
		case actionTypes.GET_BANNERS_SUCCESS: return { ...state, ...{ fetchingBanners: false, fetchBannersSuccessful: true, banners: action.banners } }

		default:return state
	}
}

export default bannersReducer
