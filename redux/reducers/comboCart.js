import { actionTypes } from '../actions/comboCart'

export const initialState = {

    addingToComboCart: false,
    addToComboCartSuccessful: false,
    comboCart: {},

    removingFromComboCartDetails: false,
    removeFromComboCartSuccessful: false,

    creatingComboCart: false,
    createComboCartSuccessful: false,

    deletingComboCart: false,
    deleteComboCartSuccessful: false,

    emptyingComboCart: false,
    emptyComboCartSuccessful: false,

	error: null
}

function customerReducer (state = initialState, action) {
	switch (action.type) {
		case actionTypes.CLEAR_ERROR: return { ...state, ...{ error: null } }
		case actionTypes.STATE_RESET: return { ...state, ...action.resetState }

		case actionTypes.ADD_TO_COMBO_CART_REQUEST: return { ...state, ...{ addingToComboCart: true } }
		case actionTypes.ADD_TO_COMBO_CART_FAILURE: return { ...state, ...{ addingToComboCart: false, error: action.error } }
		case actionTypes.ADD_TO_COMBO_CART_SUCCESS: return { ...state, ...{ addingToComboCart: false, addToComboCartSuccessful: true, comboCart: action.comboCart } }

        case actionTypes.REMOVE_FROM_COMBO_CART_REQUEST: return { ...state, ...{ removingFromComboCartDetails: true } }
		case actionTypes.REMOVE_FROM_COMBO_CART_FAILURE: return { ...state, ...{ removingFromComboCartDetails: false, error: action.error } }
		case actionTypes.REMOVE_FROM_COMBO_CART_SUCCESS: return { ...state, ...{ removingFromComboCartDetails: false, removeFromComboCartSuccessful: true, comboCart: action.comboCart } }

        case actionTypes.CREATE_COMBO_CART_REQUEST: return { ...state, ...{ creatingComboCart: true } }
		case actionTypes.CREATE_COMBO_CART_FAILURE: return { ...state, ...{ creatingComboCart: false, error: action.error } }
        case actionTypes.CREATE_COMBO_CART_SUCCESS: return { ...state, ...{ creatingComboCart: false, createComboCartSuccessful: true, comboCart: action.comboCart } }
        
        case actionTypes.DELETE_COMBO_CART_REQUEST: return { ...state, ...{ deletingComboCart: true } }
		case actionTypes.DELETE_COMBO_CART_FAILURE: return { ...state, ...{ deletingComboCart: false, error: action.error } }
        case actionTypes.DELETE_COMBO_CART_SUCCESS: return { ...state, ...{ deletingComboCart: false, deleteComboCartSuccessful: true, comboCart: action.comboCart } }
                
        case actionTypes.EMPTY_COMBO_CART_REQUEST: return { ...state, ...{ emptyingComboCart: true } }
		case actionTypes.EMPTY_COMBO_CART_FAILURE: return { ...state, ...{ emptyingComboCart: false, error: action.error } }
        case actionTypes.EMPTY_COMBO_CART_SUCCESS: return { ...state, ...{ emptyingComboCart: false, emptyComboCartSuccessful: true, comboCart: action.comboCart } }

		default:return state
	}
}

export default customerReducer
