import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { createMemoryHistory } from 'history'

import customersReducer from './customers'
import { actionTypes } from '../actions/auth'
import authReducer from './auth'
import profileReducer from './profile'
import bannersReducer from './banners'
import collectionsReducer from './collections'
import productsReducer from './products'
import generalReducer from './general'
import cartReducer from './cart'
import comboCartReducer from './comboCart'
import wishListReducer from './wishList'

const history = createMemoryHistory()
const appReducer = combineReducers({
	router: connectRouter(history),
	customers: customersReducer,
	auth: authReducer,
	profile: profileReducer,
	banners : bannersReducer,
	products: productsReducer,
	collections : collectionsReducer,
	general : generalReducer,
	cart : cartReducer,
	wishList : wishListReducer,
	comboCart : comboCartReducer
})

const rootReducer = (state, action) => {
	if (action.type === actionTypes.LOGOUT_SUCCESS) {
		return appReducer(undefined, action)
	}
	return appReducer(state, action)
}

export default rootReducer
