import styled from "styled-components";
import React, { useEffect, useState } from "react";
import Head from "next/head";
import { Col, Grid, Row, Spin, message } from "antd";

import Header from "../components/Header";
import Footer from "../components/Footer";
import ProductCard from "../components/ProductCard/ProductCard";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { API_URL } from "../constants";
import GeneralActions from "../redux/actions/general";
import { BannerTop } from "../components/Banner/Banner";
const { useBreakpoint } = Grid;

export default function Home({ children }) {
	// state = { visible: false, placement: 'left' };
	const screens = useBreakpoint();
	const [products, setProducts] = useState([]);
	const [featured, setFeatured] = useState([]);
	const [newArrivals, setNewArrivals] = useState([]);

	useEffect(() => {
		axios
			.get(API_URL + "/guest/products/?bestSale=true", {
				headers: {
					Authorization: "JWT " + localStorage.getItem("accessToken"),
				},
			})
			.then((res) => {
				setProducts(res.data.data);
			});
		axios
			.get(API_URL + "/guest/products/?isFeatured=true", {
				headers: {
					Authorization: "JWT " + localStorage.getItem("accessToken"),
				},
			})
			.then((res) => {
				setFeatured(res.data.data);
			});
		axios
			.get(API_URL + "/guest/products/?isNewArrival=true", {
				headers: {
					Authorization: "JWT " + localStorage.getItem("accessToken"),
				},
			})
			.then((res) => {
				setNewArrivals(res.data.data);
			});
	}, []);

	const addToWishList = (productId) => {
		dispatch(WishListActions.addToWishListRequest(productId));
	};

	const removeFromWishList = (productId) => {
		dispatch(WishListActions.removeFromWishListRequest(productId));
	};

	const handleClickProduct = (product) => {
		router.push(router.query.collectionId + "/" + product.slug);
	};
	const router = useRouter();
	const dispatch = useDispatch();

	const {
		error,
		fetchingBanners,
		fetchBannersSuccessful,
		banners,
		loading
	} = useSelector((state) => state.general);

	useEffect(() => {
		dispatch(GeneralActions.clearError());
	}, []);

	useEffect(() => {
		dispatch(
			GeneralActions.stateReset({
				fetchingBanners: false,
				fetchBannersSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		if (error) {
			message.error(error);
		}
	}, [error]);

	useEffect(() => {
		dispatch(GeneralActions.getBannersRequest());
	}, []);

	useEffect(() => {
		if (!fetchingBanners && fetchBannersSuccessful) {
			dispatch(
				GeneralActions.stateReset({
					fetchBannersSuccessful: false,
				})
			);
		}
	}, [fetchBannersSuccessful]);

	return (
		<Wrapper size={screens.xs ? "mobile" : "web"}>
			<Head>
				<title>TRICOTS </title>
				<meta
					name="facebook-domain-verification"
					content="qw07emt6usugfxezt4brbs0jlkxw87"
				/>
			</Head>

			<Header />
			
			<div style={{ paddingTop: "9vh" }}>
			<div className="">
				<BannerTop
					data={banners?.filter((banner) => {
						if (banner.type === "small") return banner;
					})}
				/>
			</div>
				{loading ? <div className="loading"><Spin /></div> : children}</div>

			{products?.slice(0, 4)?.length !== 0 && (
				<div className="happy-customer">Best Selling</div>
			)}
			<div className="container">
				<Row gutter={[0, 12]}>
					{products?.slice(0, 4).map((product) => {
						return (
							<Col xs={12} sm={12} md={12} l={8} xl={6}>
								<ProductCard
									addToWishList={(productId) =>
										addToWishList(productId)
									}
									removeFromWishList={(productId) =>
										removeFromWishList(productId)
									}
									data={product}
									onClick={() => handleClickProduct(product)}
								/>
							</Col>
						);
					})}
				</Row>
			</div>
			{featured?.slice(0, 4)?.length !== 0 && (
				<div className="happy-customer">Featured Products</div>
			)}
			<div className="container">
				<Row gutter={[0, 12]}>
					{featured?.slice(0, 4).map((product) => {
						return (
							<Col xs={12} sm={12} md={12} l={8} xl={6}>
								<ProductCard
									addToWishList={(productId) =>
										addToWishList(productId)
									}
									removeFromWishList={(productId) =>
										removeFromWishList(productId)
									}
									data={product}
									onClick={() => handleClickProduct(product)}
								/>
							</Col>
						);
					})}
				</Row>
			</div>
			{newArrivals?.slice(0, 4)?.length !== 0 && (
				<div className="happy-customer">New Arrivals</div>
			)}
			<div className="container">
				<Row gutter={[0, 12]}>
					{newArrivals?.slice(0, 4).map((product) => {
						return (
							<Col xs={12} sm={12} md={12} l={8} xl={6}>
								<ProductCard
									addToWishList={(productId) =>
										addToWishList(productId)
									}
									removeFromWishList={(productId) =>
										removeFromWishList(productId)
									}
									data={product}
									onClick={() => handleClickProduct(product)}
								/>
							</Col>
						);
					})}
				</Row>
			</div>
			<div className="indian-brand">HOMEGROWN INDIAN FASHION LABEL</div>

			<div className="happy-customer">
				Over{" "}
				<span className="customer-count">
					{" "}
					{
						JSON.parse(localStorage.getItem("admin"))
							?.happyCustomers
					}{" "}
				</span>{" "}
				Happy Customers
			</div>
			
			<Footer />
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.container {
		padding: 7px;
	}
	.loading {
		display:  flex;
		justify-content:  center;
		align-items: center;
		height: 300px;
	}
	.happy-customer {
		font-size: ${(props) => (props.size === "mobile" ? "18px" : "35px")};
		display: flex;
		justify-content: center;
		margin-block: 20px;
	}
	.customer-count {
		font-size: ${(props) => (props.size === "mobile" ? "18px" : "35px")};
		display: flex;
		justify-content: center;
		font-weight: bold;
		margin-inline: ${(props) => (props.size === "mobile" ? "5px" : "20px")};
		color: #800000;
	}
	.indian-brand {
		font-size: ${(props) => (props.size === "mobile" ? "15px" : "35px")};
		display: flex;
		justify-content: center;
		margin-block: 20px;
		background-color:#800000;
		color: #fff;
		padding: ${(props) => (props.size === "mobile" ? "10px" : "0px")};
	}
`;
