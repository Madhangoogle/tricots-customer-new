import styled from "styled-components";
import Themes from "../../themes/themes";
import React, { useState, useEffect } from "react";
import {
	Form,
	Input,
	Button,
	Collapse,
	Col,
	Row,
	message,
	Typography,
	Result,
	Spin,
} from "antd";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";

import Layout from "../../layouts/HomeLayout";
import {
	UserOutlined,
	LockOutlined,
	MailOutlined,
	CaretRightOutlined,
} from "@ant-design/icons";
import BreadCrumb from "../../components/BreadCrumb";
import AddressCard from "../../components/AddressCard";
import ProfileAction from "../../redux/actions/profile";
import axios from "axios";
import { API_URL } from "../../constants";
import moment from "moment";
import Head from "next/head";
import theme from "styled-theming";
import Modal from "antd/lib/modal/Modal";

const { Panel } = Collapse;
const { Text } = Typography;
export default function Profile() {
	const [order, setOrder] = useState([]);
	const [loading, setLoading] = useState(true);
	const router = useRouter();

	useEffect(() => {
		let token = localStorage.getItem("accessToken");
		if (!token) {
			message.warning("Please login before tracking order");
			router.push("/login");
		}
	}, []);
	const getOrders = () => {
		axios(API_URL + `/customer/orders/${router.query.orderId}`, {
			headers: {
				Authorization: `JWT ${localStorage.getItem("accessToken")}`,
			},
		})
			.then((res) => {
				console.log(res.data.data);
				setOrder(res.data.data);
				setLoading(false);
			})
			.catch((err) => {
				message.error(err.message);
			});
	};

	useEffect(() => {
		getOrders();
	}, [router.query.orderId]);

	const [cancelConfirm, setCancelConfirm] = useState(false);

	const handleCancelOrder = () => {
		axios
			.put(
				API_URL + `/customer/orders/${router.query.orderId}/cancel`,
				{},
				{
					headers: {
						Authorization: `JWT ${localStorage.getItem(
							"accessToken"
						)}`,
					},
				}
			)
			.then((res) => {
				console.log(res.data.data);
				message.success("Order Cancelled Successfully");
				setCancelConfirm(false);
				getOrders();
			})
			.catch((err) => {
				message.error(err.message);
				setCancelConfirm(false);
			});
	};

	return (
		<Wrapper>
			<Head>
				<title>TRICOTS </title>
				<meta
					name="facebook-domain-verification"
					content="qw07emt6usugfxezt4brbs0jlkxw87"
				/>
			</Head>
			<Layout>
				<BreadCrumb
					data={[{ name: "Home", route: "/" }, { name: "Profile" }]}
				/>
				<div className="container">
					<Modal
						onOk={() => handleCancelOrder()}
						okText="Yes"
						cancelText="No"
						onCancel={() => {
							setCancelConfirm(false);
						}}
						title="Cancel Order"
						visible={cancelConfirm}
					>
						<div className="container">
							Are you sure, you want to cancel the order ?
						</div>
					</Modal>
					{loading ? (
						<Spin />
					) : (
						<Col xs={24} lg={24}>
							<Collapse
								expandIconPosition={"right"}
								activeKey={1}
							>
								<Panel
									header={
										<Row>
											<Col xs={24} lg={12}>
												<span>Order ID:</span>
												<span className="expansion-header-data">
													{order?.orderID}
												</span>
											</Col>
											<Col xs={24} lg={12}>
												<span>Total Price:</span>
												<span className="expansion-header-data">
													₹{" "}
													{order?.totalPrice +
														(order?.deliveryCharge
															? order?.deliveryCharge
															: 0)}
												</span>
											</Col>
											<Col xs={24} lg={12}>
												<span>Status:</span>
												<span
													style={{
														marginLeft: 15,
														fontWeight: 600,
														color: Themes.black,
														// order?.status === "Ordered" ? "yellow" :Themes.green,
													}}
												>
													{order?.status}
												</span>
											</Col>
											<Col xs={24} lg={12}>
												<span>Ordered Date:</span>
												<span className="expansion-header-data">
													{moment(
														order?.createdAt
													).format("Do MMM YYYY ")}
												</span>
											</Col>
											{order?.status !== "cancelled" && (
												<>
													{" "}
													<Col xs={24} lg={12}></Col>
													<Col xs={24} lg={12}>
														<Button
															className="cancelButton"
															onClick={() =>
																setCancelConfirm(
																	true
																)
															}
														>
															Cancel
														</Button>
													</Col>
												</>
											)}
										</Row>
									}
									key="1"
								>
									{order?.trackingId && (
										<div>
											<p>
												<span>Tracking ID:</span>
												<span className="expansion-header-data">
													{order?.trackingId
														? order?.trackingId
														: "-"}
												</span>
											</p>
											<p>
												<span>Tracking Link:</span>
												<span className="expansion-header-data">
													{order?.trackingLink ? (
														<a
															href={
																order?.trackingLink
															}
														>
															{" "}
															{
																order?.trackingLink
															}{" "}
														</a>
													) : (
														"-"
													)}
												</span>
											</p>
										</div>
									)}

									<p>
										<span>Address:</span>
										<span
											style={{
												marginLeft: 15,
												fontWeight: 600,
											}}
										>
											<span className="address-name">
												<span className="special-address-text">
													{order?.address?.type}
													{`,  `}
												</span>
											</span>
											<span>
												{order?.address?.doorNumber +
													",  "}
											</span>
											<span>
												{order?.address?.street + ",  "}{" "}
											</span>
											<span>
												{order?.address?.landmark +
													",  "}{" "}
											</span>
											<span>
												{order?.address?.district +
													",  "}
											</span>
											<span>
												{order?.address?.pincode +
													",  "}
											</span>
											<div>
												{order?.address?.state +
													",  INDIA"}
											</div>
										</span>
									</p>

									{/* <p>
										<span>Payment Status:</span>
										<span
											style={{
												marginLeft: 15,
												fontWeight: 600,
												color: order?.isPaid
													? Themes.green
													: Themes.red,
											}}
										>
											{order?.isPaid ? "PAID" : "PENDING"}
										</span>
									</p> */}
									<p>
										<span>Payment Method:</span>
										<span
											style={{
												marginLeft: 15,
												fontWeight: 600,
											}}
										>
											{order?.paymentMethod === "COD"
												? "Cash on Delivery"
												: "Online"}
										</span>
									</p>

									{order?.trackingId ? (
										""
									) : (
										<p>
											<span
												style={{
													color: Themes.red,
												}}
											>
												* Tracking ID and Tracking Link
												will be updated soon.
											</span>
										</p>
									)}
									<div>
										{order?.orderItems?.map((orderItem) => {
											return (
												<Row
													style={{
														display: "flex",
														flexDirection: "row",
														marginTop: 20,
														border:
															"1px solid #000",
														padding: 10,
													}}
													align="middle"
													gutter={[0, 24]}
												>
													<Col xs={8} md={4}>
														<img
															src={
																orderItem?.imageUrl
															}
															style={{
																width: 50,
																height: 50,
															}}
														/>
													</Col>
													<Col xs={16} md={8}>
														<span
															style={{
																color:
																	Themes.primary,
																fontSize: 14,
																fontWeight: 400,
															}}
														>
															{orderItem?.productName
																? orderItem?.productName
																: orderItem.inventoryName}
														</span>
													</Col>
													<Col xs={6} md={4}>
														<div
															style={{
																backgroundColor:
																	orderItem?.color,
																height: 33,
																width: 33,
																borderRadius: 50,
																marginRight: 10,
																cursor:
																	"pointer",
																border:
																	"2px solid #000",
															}}
														/>
													</Col>
													<Col xs={9} md={4}>
														<Text
															style={{
																fontSize: 12,
															}}
														>
															Size{" "}
															<span
																style={{
																	fontSize: 12,
																	fontWeight:
																		"700",
																}}
															>
																(
																{
																	orderItem?.size
																}
																)
															</span>{" "}
														</Text>
													</Col>
													{/* <Col xs={2} md={2}>
																<span
																	style={{
																		color: Themes.backgroundMain,
																		fontSize: 14,
																		fontWeight: 400,
																	}}
																>
																	X
																</span>
															</Col> */}
													<Col xs={9} md={4}>
														<Text
															style={{
																fontSize: 12,
															}}
														>
															Quantity{" "}
															<span
																style={{
																	fontSize: 12,
																	fontWeight:
																		"700",
																}}
															>
																(
																{
																	orderItem?.quantity
																}
																)
															</span>{" "}
														</Text>
													</Col>
													{/* <Col xs={6} md={2}>
																<span
																	style={{
																		color: Themes.backgroundMain,
																		fontSize: 14,
																		fontWeight: 400,
																	}}
																>
																	{" "}
																	₹{" "}
																	{orderItem?.specialPrice
																		? orderItem?.specialPrice !== 0
																			? orderItem?.specialPrice *
																			  orderItem?.quantity
																			: orderItem?.price * orderItem?.quantity
																		: null}
																</span>
															</Col> */}

													<span></span>
												</Row>
											);
										})}
									</div>
									<div>
										{Object.keys(
											order?.combo ? order?.combo : {}
										).map((comboId) => {
											return (
												<Row
													style={{
														display: "flex",
														flexDirection: "row",
														marginTop: 20,
														border:
															"1px solid #000",
														padding: 10,
													}}
													align="middle"
													gutter={[0, 12]}
												>
													<div
														style={{
															display: "flex",
															width: "100%",
														}}
													>
														Combo Name:{" "}
														{order.combo[comboId] &&
															order.combo[comboId]
																.length !== 0 &&
															order.combo[
																comboId
															][0].comboName}
													</div>
													<div
														style={{
															display: "flex",
															width: "100%",
														}}
													>
														Price:{" "}
														{order.combo[comboId] &&
															order.combo[comboId]
																.length !== 0 &&
															order.combo[
																comboId
															][0].comboPrice}
													</div>
													{order.combo[comboId].map(
														(orderItem) => {
															return (
																<>
																	<Col
																		xs={6}
																		lg={6}
																	>
																		<img
																			src={
																				orderItem.imageUrl
																			}
																			style={{
																				width: 50,
																				height: 50,
																			}}
																		/>
																	</Col>
																	<Col
																		xs={10}
																		lg={6}
																	>
																		<span
																			style={{
																				color:
																					Themes.primary,
																				fontSize: 14,
																				fontWeight: 400,
																			}}
																		>
																			{
																				orderItem?.colorName
																			}
																		</span>
																	</Col>
																	<Col
																		xs={6}
																		lg={6}
																	>
																		<div
																			style={{
																				backgroundColor:
																					orderItem?.color,
																				height: 33,
																				width: 33,
																				borderRadius: 50,
																				marginRight: 10,
																				cursor:
																					"pointer",
																				border:
																					"2px solid #000",
																			}}
																		/>
																	</Col>
																	<Col
																		xs={2}
																		lg={6}
																	>
																		<Text
																			style={{
																				fontWeight:
																					"900",
																			}}
																		>
																			{
																				orderItem?.size
																			}
																		</Text>
																	</Col>

																	<span></span>
																</>
															);
														}
													)}
												</Row>
											);
										})}
									</div>
								</Panel>
							</Collapse>
						</Col>
					)}
				</div>
			</Layout>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	.container {
		width: 100%;
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: column;
		/* background-color: ${Themes.backgroundMain}; */
	}
	.register-form {
		/* max-width: 350px; */
	}
	.register-form-forgot {
		float: right;
	}
	.ant-col-rtl .register-form-forgot {
		float: left;
	}
	.register-form-button {
		width: 100%;
	}
	.register-form-button {
		width: 100%;
	}
	[data-theme="compact"]
		.site-collapse-custom-collapse
		.site-collapse-custom-panel,
	.site-collapse-custom-collapse1 .site-collapse-custom-panel {
		overflow: hidden;
		background: #ffffff;
		border: 0px;
		border-radius: 2px;
		width: 90vw;
	}
	.back-btn {
		border: 1px solid ${Themes.primary};
		color: ${Themes.primary};
		width: 100%;
	}
	.cancelButton {
		background-color: ${Themes.negative};
		color: ${Themes.white};
		width: 100%;
		margin-block: 10px;
		width: 50%;
		/* border-radius:  25px; */
	}
	.expansion-header-data {
		margin-left: 15;
		font-weight: 600;
	}
`;
