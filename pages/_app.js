import "../styles/globals.css";
import "antd/dist/antd.css";
// import "react-checkbox-tree/lib/react-checkbox-tree.css";
// import "react-perfect-scrollbar/dist/css/styles.css";
// import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

import "../styles/antd.less";
import { ThemeProvider } from "styled-components";
import withReduxSaga from "next-redux-saga";
import withReduxStore from "../lib/with-redux-store";
import { persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";
// import SideMenu from "../components/SideMenu";
import { useEffect, useState } from "react";
import Router from "next/router";
import { Provider, useSelector } from "react-redux";
import { onNavigate } from "../util/navigation";
import PropTypes from "prop-types";
import { API_URL } from "../constants";
import axios from "axios";
import moment from "moment"

function MyApp(props) {
	MyApp.propTypes = {
		reduxStore: PropTypes.any,
		router: PropTypes.any,
	};

	const { reduxStore, router } = props;
	const persistor = persistStore(reduxStore);
	const [admin, setAdmin] = useState();

	const [renderPage, setRenderPage] = useState(false);
	useEffect(() => {
		// const getInitialProps = async ({ Component, ctx }) => {
		//   let pageProps = {}
		//   if (Component.getInitialProps) {
		//     pageProps = await Component.getInitialProps({ ctx })
		//   }
		//   return { pageProps }
		// }
		// const token = localStorage.getItem('accessToken')
		// if (token) {
		// 	if (router.pathname === '/login') {
		// 		onNavigate(Router, '/')
		// 		setRenderPage(true)
		// 	} else {
		// 		setRenderPage(true)
		// 	}
		// } else {
		// 	if (router.pathname === '/login') {
		// 		setRenderPage(true)
		// 	} else {
		// 		setRenderPage(true)
		// 		onNavigate(Router, "/login");
		// 	}
		// }

		Router.events.on("routeChangeComplete", () => {
			window.scroll({
				top: 0,
				left: 0,
				behavior: "smooth",
			});
		});
		import("react-facebook-pixel")
			.then((x) => x.default)
			.then((ReactPixel) => {
				ReactPixel.init("482658696105407");
				ReactPixel.pageView();
				Router.events.on("routeChangeComplete", () => {
					ReactPixel.pageView();
				});
		});
	});

	const getAdmin = () => {
		axios.get(`${API_URL}/guest/admins`, {
				headers: {
					Authorization: `JWT ${localStorage.getItem("accessToken")}`,
				},
			})
			.then(async (res) => {
				setAdmin(res.data.data);
				localStorage.setItem("admin", JSON.stringify(res.data.data));
				setRenderPage(true);
			});
	};

	useEffect(() => {
		getAdmin();
		// if (!adminDetails  || moment().format("DD-MM-YYYY").toString() !== date) {
		// 	getAdmin();
		// } else {
		// 	setRenderPage(true);
		// }
	}, []);

	// eslint-disable-next-line react/prop-types
	// const { Component, pageProps } = props
	return (
		<Provider store={reduxStore}>
			<ThemeProvider theme={{ mode: "light" }}>
				<PersistGate
					loading={<div>Loading.........</div>}
					persistor={persistor}
				>
					{renderPage ? <AppCore {...props} /> : null}
				</PersistGate>
			</ThemeProvider>
		</Provider>
	);
}

const AppCore = (props) => {
	// eslint-disable-next-line react/prop-types
	const { Component, pageProps } = props;

	return (
		<div
			style={{
				display: "flex",
				flexDirection: "row",
				alignItems: "flex-start",
			}}
		>
			<Component {...pageProps} />
		</div>
	);
};

export default withReduxStore(withReduxSaga(MyApp));
