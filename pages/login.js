import styled from "styled-components";
import Themes from "../themes/themes";
import React, { useEffect, useState } from "react";
import Head from "next/head";
import { Form, Input, Button, Checkbox, message, Row, Col } from "antd";
import {
	UserOutlined,
	LockOutlined,
	GoogleCircleFilled,
	GoogleOutlined,
} from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";

import Layout from "../layouts/HomeLayout";
import BreadCrumb from "../components/BreadCrumb";
import AuthActions from "../redux/actions/auth";
import GoogleLogin from "react-google-login";
import Images from "../assests/index";
import OtpInput from "react-otp-input";
import firebase from "firebase";

if (!firebase.apps.length) {
	firebase.initializeApp({
		apiKey: "AIzaSyDxus9ZzHBNIDWR46Q3J1ZPqpn8c5gb8wM",
		authDomain: "tricots-93735.firebaseapp.com",
		databaseURL: "https://tricots-93735.firebaseio.com",
		projectId: "tricots-93735",
		storageBucket: "tricots-93735.appspot.com",
		messagingSenderId: "850629670848",
		appId: "1:850629670848:web:35e9b3c73306962572fa25",
		measurementId: "G-TPZVBT4478",
	});
} else {
	firebase.app();
}

export default function Login() {
	const [loadingOTP, setSubmitLoading] = useState(false);

	const router = useRouter();
	const dispatch = useDispatch();
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [phoneNumber, setPhoneNumber] = useState("");
	const [code, setCode] = useState();
	const [isModalVisible, setIsModalVisible] = useState(true);
	const [codeSent, setCodeSent] = useState(false);

	const {
		error,
		loggingIn,
		logInSuccessful,
		signingUp,
		signupSuccessful,
		user,
	} = useSelector((state) => state.auth);

	useEffect(() => {
		dispatch(AuthActions.clearError());
	}, []);
	useEffect(() => {
		let token = localStorage.getItem("accessToken");
		if (token) {
			message.success("Already Logged In, Enjoy Shopoing");
			router.push("/");
		}
	}, []);

	useEffect(() => {
		dispatch(
			AuthActions.stateReset({
				logInSuccessful: false,
				logInSuccessful: false,
				signupSuccessful: false,
				signingUp: false,
			})
		);
	}, []);

	useEffect(() => {
		if (error === "Please check your username and/or password.") {
			console.log(error);
			dispatch(
				AuthActions.signupRequest({
					emailId: username,
					password: password,
					confirm: password,
					firstName: firstName,
					lastName: lastName,
				})
			);
		} else if (error) {
			message.error(error);
			dispatch(AuthActions.clearError());
		}
	}, [error]);

	useEffect(() => {
		if (!signingUp && signupSuccessful) {
			dispatch(
				AuthActions.stateReset({
					signupSuccessful: false,
				})
			);
			dispatch(AuthActions.loginRequest(username, password));
			message.success("Registered Successfully");
		}
	}, [signupSuccessful]);

	useEffect(() => {
		if (!loggingIn && logInSuccessful) {
			dispatch(
				AuthActions.stateReset({
					logInSuccessful: false,
				})
			);
			router.back();
			message.success("Logged in Successfully");
		}
	}, [logInSuccessful]);

	const onFinish = ({ username, password }) => {
		dispatch(AuthActions.loginRequest(username, password));
	};
	const responseGoogle = (response) => {
		setUsername(response?.profileObj?.email);
		dispatch(AuthActions.loginRequest(username, "12345678"));
		setPassword("12345678");
		setFirstName(response?.profileObj?.givenName);
		setLastName(response?.profileObj?.familyName);

		dispatch(
			AuthActions.loginRequest(response?.profileObj?.email, "12345678")
		);
	};
	const [timer, setTimer] = useState(60);

	useEffect(() => {
		if (timer < 60 && timer !== 0) {
			const timerId = setInterval(() => setTimer(timer - 1), 1000);
			return () => clearInterval(timerId);
		} else {
			setTimer(60);
		}
	}, [timer]);

	const StopTick = () => {
		setTimer(60);
	};

	const handleOk = (type) => {
		if (!codeSent) {
			setSubmitLoading(true);
			window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
				"recaptcha-container",
				{
					size: "invisible",
					// other options
				}
			);
			const appVerifier = window.recaptchaVerifier;
			firebase
				.auth()
				.signInWithPhoneNumber("+91" + phoneNumber, appVerifier)
				.then((confirmResult) => {
					console.log(confirmResult, "LLLLLLLLLLLLLLLLL");
					// success
					window.confirmationResult = confirmResult;
					setCodeSent(true);
					setSubmitLoading(false);
					setTimer(59);
				})
				.catch((error) => {
					console.log(error, "MMMMMMMMMMMMMMM");
					// error
					window.recaptchaVerifier.render().then(function (widgetId) {
						grecaptcha.reset(widgetId);
					});
					setSubmitLoading(false);
				});
		} else {
			setSubmitLoading(true);
			if (type === "resend") {
				setSubmitLoading(false);
				const appVerifier = window.recaptchaVerifier;
				firebase
				.auth()
				.signInWithPhoneNumber("+91" + phoneNumber, appVerifier)
				.then((confirmResult) => {
					window.confirmationResult = confirmResult;
				})
				.catch((error) => {
					window.recaptchaVerifier.render().then(function (widgetId) {
						grecaptcha.reset(widgetId);
					});
				});
			}
			confirmationResult
				.confirm(code)
				.then(async (result) => {
					const user = result.user;
					var token = await firebase.auth().currentUser.getIdToken();
					dispatch(AuthActions.loginRequest(token));
					setSubmitLoading(false);
					StopTick();
				})
				.catch((error) => {
					message.error("Invalid code");
					setSubmitLoading(false);
				});
		}
	};

	return (
		<Wrapper>
			<Head>
				<title>TRICOTS </title>
				<meta
					name="facebook-domain-verification"
					content="qw07emt6usugfxezt4brbs0jlkxw87"
				/>
			</Head>
			<Layout>
				<BreadCrumb
					data={[{ name: "Home", route: "/" }, { name: "Login" }]}
				/>
				<div className="container">
					<p>LOGIN</p>
					<Form
						name="normal_login"
						className="login-form"
						initialValues={{
							remember: true,
						}}
						onFinish={() => {}}
					>
						{/* <Form.Item
							name="username"
							rules={[
								{
									required: true,
									message: "Please input your Username!",
								},
							]}
						>
							<Input
								prefix={
									<UserOutlined className="site-form-item-icon" />
								}
								placeholder="Email"
							/>
						</Form.Item>
						<Form.Item
							name="password"
							rules={[
								{
									required: true,
									message: "Please input your Password!",
								},
							]}
						>
							<Input
								prefix={
									<LockOutlined className="site-form-item-icon" />
								}
								type="password"
								placeholder="Password"
							/>
						</Form.Item>
						<Form.Item>
							<Form.Item
								name="remember"
								valuePropName="checked"
								noStyle
							>
								<Checkbox>Remember me</Checkbox>
							</Form.Item>

						</Form.Item>

						<Form.Item>
							<Button
								loading={loggingIn}
								type="primary"
								htmlType="submit"
								className="login-form-button"
							>
								Log in
							</Button>
							Or <a href="/signup">register now!</a>
						</Form.Item> */}

						{/* <GoogleLogin
							render={(renderProps) => (
								<Button
									className="login-form-button"
									icon={
										<img
											style={{
												width: 20,
												height: 20,
												marginRight: 20,
												objectFit: "contain",
											}}
											src={Images.google}
										/>
									}
									onClick={renderProps.onClick}
									disabled={renderProps.disabled}
								>
									Login with Google
								</Button>
							)}
							disabledStyle={{ backgroundColor: "red" }}
							isSignedIn={true}
							// jsSrc={"https://apis.google.com/js/api.js"}
							clientId="711691814112-tepb25r7b4dj2itsuhlumtiuehtbjflg.apps.googleusercontent.com"
							buttonText="Login with Google"
							onSuccess={responseGoogle}
							onFailure={responseGoogle}
							cookiePolicy={"single_host_origin"}
						></GoogleLogin> */}
						<div id="recaptcha-container"> </div>
						{!codeSent ? (
							<Row justify="space-between">
								<Col span={4}>
									<Input value={"+91"} />
								</Col>
								<Col span={18}>
									<Input
										placeholder={"Phone number"}
										onChange={(v) =>
											setPhoneNumber(v.target.value)
										}
									/>
								</Col>
								<Col span={24}>
									<Button
										onClick={handleOk}
										loading={loadingOTP}
										style={{ marginTop: 20 }}
										type="primary"
										className="login-form-button"
									>
										Submit
									</Button>
								</Col>
							</Row>
						) : (
							<Row justify="space-between">
								<OtpInput
									value={code}
									inputStyle={{ width: "80%" }}
									onChange={(otp) => setCode(otp)}
									numInputs={6}
									separator={<span>{``}</span>}
								/>
								<Col span={24}>
									<Button
										onClick={handleOk}
										loading={loggingIn || loadingOTP}
										style={{ marginTop: 20 }}
										type="primary"
										className="login-form-button"
									>
										Verify OTP
									</Button>
								</Col>
								{codeSent && timer === 60 ? (
								<Col span={24}>
									<div
										style={{
											color: "blue",
											cursor: "pointer",
										}}
										onClick={() => {
											setCodeSent(true);
											message.success("Code Sent");
											setTimer(59);
											handleOk("resend");
										}}
									>
										resend code ?
									</div>
								</Col>
							) : (
								<Col span={23}>
									{codeSent && (
										<div style={{ color: "blue" }}>
											{timer + " s"}
										</div>
									)}
								</Col>
							)}
							</Row>
						)}
						{/* <Button
							onClick={handleOk}
							loading={loggingIn || loadingOTP}
							style={{ marginTop: 20 }}
							type="primary"
							htmlType="submit"
							className="login-form-button"
						>
							{!codeSent ? "Submit" : "Verify OTP"}
						</Button> */}
					</Form>
				</div>
			</Layout>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.container {
		padding: 20px;
		display: flex;
		justify-content: center;
		align-items: center;
		flex-direction: column;
		/* background-color: ${Themes.backgroundMain}; */
	}
	.login-form {
		max-width: 350px;
	}
	.login-form-forgot {
		float: right;
	}
	.ant-col-rtl .login-form-forgot {
		float: left;
	}
	.login-form-button {
		width: 100%;
	}
	.login-form-button {
		width: 100%;
	}
`;
