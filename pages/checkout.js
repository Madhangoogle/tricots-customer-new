import styled from "styled-components";
import Themes from "../themes/themes";
import React, { useState, useEffect, useDebugValue } from "react";
import {
	Row,
	Col,
	Space,
	Dropdown,
	Carousel,
	Drawer,
	Result,
	Button,
	Radio,
	Collapse,
	Grid,
	message,
	Alert,
	Modal,
	Input,
} from "antd";
import Theme from "../themes/themes";
import Head from "next/head";
import Images from "../assests";
import { TagFilled } from "@ant-design/icons";

import Header from "../components/Header";
import Footer from "../components/Footer";
import Categories from "../components/Categories";
import Collections from "../components/Collections";
import { BannerTop, BannerBottom } from "../components/Banner";
import HomeLayout from "../layouts/HomeLayout";
import { CartItem, ComboCartItem } from "../components/CartItem";
import AddressCard from "../components/AddressCard";
import BreadCrumb from "../components/BreadCrumb";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import generalActions from "../redux/actions/general";
import cartActions from "../redux/actions/cart";
import ProfileAction from "../redux/actions/profile";
import Razorpay from "razorpay";
import comboCartartActions from "../redux/actions/comboCart";

import axios from "axios";
import { API_URL } from "../constants";
import firebase from "firebase";
import OtpInput from "react-otp-input";
import AuthActions from "../redux/actions/auth";

const { Panel } = Collapse;
const { useBreakpoint } = Grid;
const callback = () => {};
if (!firebase.apps.length) {
	firebase.initializeApp({
		apiKey: "AIzaSyDxus9ZzHBNIDWR46Q3J1ZPqpn8c5gb8wM",
		authDomain: "tricots-93735.firebaseapp.com",
		databaseURL: "https://tricots-93735.firebaseio.com",
		projectId: "tricots-93735",
		storageBucket: "tricots-93735.appspot.com",
		messagingSenderId: "850629670848",
		appId: "1:850629670848:web:35e9b3c73306962572fa25",
		measurementId: "G-TPZVBT4478",
	});
} else {
	firebase.app(); // if already initialized, use that one
}

export default function Home() {
	// state = { visible: false, placement: 'left' };

	const [loadingOTP, setSubmitLoading] = useState(false);

	function loadScript(src) {
		return new Promise((resolve) => {
			const script = document.createElement("script");
			script.src = src;
			script.onload = () => {
				resolve(true);
			};
			script.onerror = () => {
				resolve(false);
			};
			document.body.appendChild(script);
		});
	}
	console.log(appliedCode);

	const screens = useBreakpoint();
	const dispatch = useDispatch();
	const router = useRouter();
	const { error, addresses, profilDetails } = useSelector(
		(state) => state.profile
	);
	const [total, setTotal] = useState(0);
	const [discount, setDiscount] = useState(0);
	const [totalPayable, setTotalPayable] = useState(0);
	const [cartTotal, setCartTotal] = useState(0);
	const [shipping, setShipping] = useState(0);
	const [selectedAddress, setSelectedAddress] = useState();
	const [radio, setRadio] = useState("BOTH");
	const [loading, setLoading] = useState(false);
	const admin = JSON.parse(localStorage.getItem("admin"));
	const [promocodes, setPromocodes] = useState();
	const [appliedCode, setSetAppliedCode] = useState();
	const [appliedPromocodeId, setSetAppliedCodeId] = useState();
	const [phoneNumber, setPhoneNumber] = useState("");
	const [code, setCode] = useState();
	const [isModalVisible, setIsModalVisible] = useState(true);
	const [codeSent, setCodeSent] = useState(false);

	useEffect(() => {
		let token = localStorage.getItem("accessToken");
		if (!token || token == null) {
			// message.warning("Please login before placing order");
			// router.push("/login");
			setIsModalVisible(true);
		} else {
			setIsModalVisible(false);
		}
	}, []);

	useEffect(() => {
		dispatch(ProfileAction.clearError());
	}, []);

	useEffect(() => {
		dispatch(
			ProfileAction.stateReset({
				updatingProfileDetails: false,
				updateProfileDetailsSuccessful: false,
				fetchingProfileDetails: false,
				fetchProfileDetailsSuccessful: false,
				fetchingAddresses: false,
				fetchAddressesSuccessful: false,
			})
		);
		dispatch(
			comboCartartActions.stateReset({
				addingToComboCart: false,
				addToComboCartSuccessful: false,

				removingFromComboCart: false,
				removeFromComboCartSuccessful: false,

				creatingComboCart: false,
				createComboCartSuccessful: false,

				deletingComboCart: false,
				deleteComboCartSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		if (error) {
			dispatch(ProfileAction.clearError());
			message.error(error);
		}
	}, [error]);

	const {
		cart,
		addingToCart,
		addToCartSuccessful,

		removingFromCart,
		removeFromCartSuccessful,

		creatingCart,
		createCartSuccessful,

		deletingCart,
		deleteCartSuccessful,
	} = useSelector((state) => state.cart);

	const {
		comboCart,
		addingToComboCart,
		addToComboCartSuccessful,

		removingFromComboCart,
		removeFromComboCartSuccessful,

		creatingComboCart,
		createComboCartSuccessful,

		deletingComboCart,
		deleteComboCartSuccessful,
	} = useSelector((state) => state.comboCart);

	useEffect(() => {
		if (
			createCartSuccessful ||
			addToCartSuccessful ||
			removeFromCartSuccessful ||
			deleteCartSuccessful ||
			addingToComboCart ||
			addToComboCartSuccessful ||
			removingFromComboCart ||
			removeFromComboCartSuccessful ||
			creatingComboCart ||
			createComboCartSuccessful ||
			deletingComboCart ||
			deleteComboCartSuccessful
		) {
			dispatch(
				cartActions.stateReset({
					addingToCart: false,
					addToCartSuccessful: false,

					removingFromCartDetails: false,
					removeFromCartSuccessful: false,

					creatingCart: false,
					createCartSuccessful: false,

					deletingCart: false,
					deleteCartSuccessful: false,
				})
			);
			dispatch(
				comboCartartActions.stateReset({
					addingToComboCart: false,
					addToComboCartSuccessful: false,

					removingFromComboCart: false,
					removeFromComboCartSuccessful: false,

					creatingComboCart: false,
					createComboCartSuccessful: false,

					deletingComboCart: false,
					deleteComboCartSuccessful: false,
				})
			);
		}
	}, [
		createCartSuccessful,
		addToCartSuccessful,
		removeFromCartSuccessful,
		deleteCartSuccessful,
		addToComboCartSuccessful,
		createComboCartSuccessful,
		removeFromComboCartSuccessful,
		deleteComboCartSuccessful,
	]);

	useEffect(() => {
		if (!isModalVisible) {
			dispatch(ProfileAction.getAddressesRequest());
			dispatch(ProfileAction.getProfileDetailsRequest());
			getPromocodes();
		}
	}, [isModalVisible]);

	useEffect(() => {
		if (router.query.productId)
			dispatch(
				generalActions.getProductDetailsRequest(router.query.productId)
			);
	}, [router.query.productId]);

	const handleClickPlus = (inventory, quantity) => {
		if (!cart || !cart[inventory._id]) {
			dispatch(
				cartActions.createCartRequest({
					[inventory._id]: {
						...inventory,
						productName: productDetails.name,
						specialPrice: productDetails.specialPrice,
						defaultPrice: productDetails.defaultPrice,
						quantity: quantity,
					},
				})
			);
		} else {
			dispatch(cartActions.addToCartRequest(inventory._id, quantity + 1));
		}
	};

	const handleClickMinus = (inventory, quantity) => {
		if (quantity === "delete") {
			return dispatch(cartActions.deleteCartRequest(inventory._id));
		}
		if (cart && cart[inventory._id] && cart[inventory._id].quantity === 1) {
			dispatch(cartActions.deleteCartRequest(inventory._id));
		} else {
			dispatch(cartActions.addToCartRequest(inventory._id, quantity - 1));
		}
	};

	const deleteComboCart = (inventory, quantity) => {
		if (quantity === "delete") {
			return dispatch(
				comboCartartActions.deleteComboCartRequest(inventory.comboId)
			);
		}
	};

	useEffect(() => {
		if (
			createCartSuccessful ||
			addToCartSuccessful ||
			removeFromCartSuccessful ||
			deleteCartSuccessful
		) {
			dispatch(
				cartActions.stateReset({
					addToCartSuccessful: false,
					removeFromCartSuccessful: false,
					createCartSuccessful: false,
					deleteCartSuccessful: false,
				})
			);
		}
	}, [
		createCartSuccessful,
		addToCartSuccessful,
		removeFromCartSuccessful,
		deleteCartSuccessful,
	]);

	useEffect(() => {
		dispatch(cartActions.clearError());
	}, []);

	useEffect(() => {
		if (!creatingCart && createCartSuccessful) {
			cartActions.stateReset({
				createCartSuccessful: false,
			});
			message.success("Item added to cart successfully");
		}
	}, [createCartSuccessful]);

	useEffect(() => {
		if (!deletingCart && deleteCartSuccessful) {
			cartActions.stateReset({
				deleteCartSuccessful: false,
			});
			message.success("Cart updated successfully");
		}
	}, [deleteCartSuccessful]);

	useEffect(() => {
		if (!addingToCart && addToCartSuccessful) {
			cartActions.stateReset({
				addToCartSuccessful: false,
			});
			message.success("Item in cart updated successfully");
		}
	}, [addToCartSuccessful]);

	useEffect(() => {
		if (!removingFromCart && removeFromCartSuccessful) {
			cartActions.stateReset({
				removeFromCartSuccessful: false,
			});
			message.success("Item in a cart removed successfully");
		}
	}, [removeFromCartSuccessful]);
	useEffect(() => {
		var total = 0;
		var discount = 0;
		var cartTotal = 0;
		var shipping = parseFloat(
			radio === "ONLINE" || radio === "BOTH" ? 0 : admin.deliveryCharge
		);
		var totalPayable = 0;
		Object.keys(cart).map((item) => {
			total += cart[item].defaultPrice * cart[item].quantity;
			discount +=
				(cart[item].defaultPrice -
					(cart[item].specialPrice ? cart[item].specialPrice : 0)) *
				cart[item].quantity;
			cartTotal +=
				(cart[item].specialPrice
					? cart[item].specialPrice
					: cart[item].defaultPrice) * cart[item].quantity;
		});
		Object.keys(comboCart).map((item) => {
			total += parseFloat(comboCart[item].defaultPrice);
			discount += parseFloat(
				comboCart[item].defaultPrice -
					(comboCart[item].specialPrice
						? comboCart[item].specialPrice
						: 0)
			);
			cartTotal += parseFloat(
				comboCart[item].specialPrice
					? comboCart[item].specialPrice
					: comboCart[item].defaultPrice
			);
		});
		totalPayable = cartTotal + shipping;
		if (appliedCode) {
			if (
				promocodes?.filter((code) => {
					if (
						totalPayable <= code.max &&
						totalPayable >= code.min &&
						(radio === code.customerType ||
							code.customerType === "BOTH")
					)
						return code;
				}).length !== 0
			) {
				totalPayable -= appliedCode?.deductedPrice;
			} else {
				setSetAppliedCode();
				setSetAppliedCodeId();
			}
		}

		setTotal(total);
		setDiscount(discount);
		setCartTotal(cartTotal);
		setShipping(shipping);
		setTotalPayable(totalPayable);
		if(Object.keys(cart).length === 0 && Object.keys(comboCart).length ===0 ){
			router.back()
		}
	}, [
		cart,
		createCartSuccessful,
		addToCartSuccessful,
		removeFromCartSuccessful,
		deleteCartSuccessful,
		addingToComboCart,
		addToComboCartSuccessful,
		removingFromComboCart,
		removeFromComboCartSuccessful,
		creatingComboCart,
		createComboCartSuccessful,
		deletingComboCart,
		deleteComboCartSuccessful,
		appliedCode,
		radio,
	]);

	useEffect(() => {
		setSetAppliedCodeId();
		setSetAppliedCode();
	}, [radio]);
	const handlePlaceOrder = async () => {
		setLoading(true);
		if (!selectedAddress) {
			setLoading(false);
			return message.warn("Select address to proceed");
		}
		if (radio === "BOTH" || radio === undefined) {
			setLoading(false);
			return message.warn("Select payment mode to proceed");
		}
		let inventories = [];
		await Promise.all(
			Object.keys(cart)?.map((cartItem) => {
				inventories.push({
					inventoryId: cartItem,
					quantity: cart[cartItem].quantity,
				});
			})
		);
		const data = {
			inventories: inventories,
			comboCart: comboCart,
			paymentMethod: radio,
			address: selectedAddress,
			deliveryCharge: shipping,
			totalPrice: cartTotal,
			promocodeId: appliedPromocodeId,
		};

		axios
			.post(`${API_URL}/customer/orders`, data, {
				headers: {
					authorization: `JWT ${await localStorage.getItem(
						"accessToken"
					)}`,
				},
			})
			.then((result) => {
				if (result.data.apiStatus === "SUCCESS") {
					dispatch(cartActions.emptyCartRequest());
					dispatch(comboCartartActions.emptyComboCartRequest());
					message.success("Order Placed Successfully");
					// setThanksModal(result.data?.data);
					router.push("/order/" + result?.data?.data?._id);
					// router.push("track-order");
				} else {
					message.error("FAILED to place order");
				}
				setLoading(false);
			})
			.catch((error) => {
				message.error(error.message);
				setLoading(false);
			});
	};

	async function displayRazorpay() {
		setLoading(true);

		if (!selectedAddress) {
			setLoading(false);
			return message.warn("Select address to proceed");
		}
		const res = await loadScript(
			"https://checkout.razorpay.com/v1/checkout.js"
		);

		if (!res) {
			alert("Razorpay SDK failed to load. Are you online?");
			return;
		}

		let totalAmount = totalPayable;
		// if (promocodeVerified) {
		// 	totalAmount = totalAmount - (total * promocode.discountPercentage) / 100;
		// }
		// if (radio === "cashOnDelivery") {
		// 	totalAmount += totalAmount + 40;
		// }

		axios
			.post(
				`${API_URL}/customer/orders/create`,
				{
					amount: totalAmount,
					currency: "INR",
				},
				{
					headers: {
						authorization: `JWT ${await localStorage.getItem(
							"accessToken"
						)}`,
					},
				}
			)
			.then((result) => {
				setLoading(false);

				if (!result) {
					alert("Server error. Are you online?");
					return;
				}

				const { amount, id: order_id, currency } = result.data.data;

				const options = {
					key: "rzp_live_gH57Oe7loTCfqb", // Enter the Key ID generated from the Dashboard
					amount: (10).toString(),
					currency: currency,
					name: "Tricots.in",
					description: "Appearel",
					image: JSON.stringify(localStorage.getItem("admin"))
						.logoUrl,
					order_id: order_id,
					handler: async function (response) {
						let inventories = [];
						await Promise.all(
							Object.keys(cart)?.map((cartItem) => {
								inventories.push({
									inventoryId: cartItem,
									quantity: cart[cartItem].quantity,
								});
							})
						);
						const data = {
							inventories: inventories,
							paymentMethod: "ONLINE",
							address: selectedAddress,
							deliveryCharge: shipping,
							totalPrice: cartTotal,
							paymentDetails: {
								orderCreationId: order_id,
								razorpayPaymentId: response.razorpay_payment_id,
								razorpayOrderId: response.razorpay_order_id,
								razorpaySignature: response.razorpay_signature,
							},
							comboCart: comboCart,
							promocodeId: appliedPromocodeId,
						};

						setLoading(true);

						axios
							.post(`${API_URL}/customer/orders`, data, {
								headers: {
									authorization: `JWT ${await localStorage.getItem(
										"accessToken"
									)}`,
								},
							})
							.then((result) => {
								if (result.data.apiStatus === "SUCCESS") {
									setLoading(false);
									dispatch(cartActions.emptyCartRequest());
									dispatch(
										comboCartartActions.emptyComboCartRequest()
									);
									message.success(
										"Order Placed Successfully"
									);
									// setThanksModal(result.data?.data);
									router.push(
										"/order/" + result?.data?.data?._id
									);
									// router.push("track-order");

									// return router.push("track-order");
								} else {
									// result.error.apiStatus === "FAILURE";
									setLoading(false);
									message.error("FAILED to place order");
								}
							})
							.catch((error) => {
								message.error(error.message);
								setLoading(false);
							});
					},
					prefill: {
						name:
							profilDetails?.firstName +
							" " +
							profilDetails?.lastName,
						email: profilDetails?.emailId,
						contact: profilDetails?.phoneNumber,
					},
					notes: {
						address:
							selectedAddress?.doorNumber +
							", " +
							selectedAddress?.street +
							", " +
							selectedAddress?.landmark +
							", ",
					},
					theme: {
						color: Theme.primary,
					},
				};

				const paymentObject = new window.Razorpay(options);
				paymentObject.open();
			})
			.catch((error) => {
				message.error(error.message);
				setLoading(false);
			});
	}

	const getPromocodes = () => {
		axios
			.get(`${API_URL}/guest/promocodes`, {
				headers: {
					Authorization: `JWT ${localStorage.getItem("accessToken")}`,
				},
			})
			.then((res) => {
				setPromocodes(res.data.data);
			});
	};
	const verifyPromocode = (code) => {
		axios
			.post(
				`${API_URL}/guest/promocodes/${code._id}`,
				{ total: cartTotal, customerType: radio, code: code.code },
				{
					headers: {
						Authorization: `JWT ${localStorage.getItem(
							"accessToken"
						)}`,
					},
				}
			)
			.then((res) => {
				message.success("Promocode Applied");
				setSetAppliedCode(res.data.data);
				setSetAppliedCodeId(code._id);
			});
	};
	const [timer, setTimer] = useState(60);

	useEffect(() => {
		if (timer < 60 && timer !== 0) {
			const timerId = setInterval(() => setTimer(timer - 1), 1000);
			return () => clearInterval(timerId);
		} else {
			setTimer(60);
		}
	}, [timer]);

	const StopTick = () => {
		setTimer(60);
	};

	const handleOk = (type) => {
		if (!codeSent) {
			setSubmitLoading(true);
			window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
				"recaptcha-container",
				{
					size: "invisible",
					// other options
				}
			);
			const appVerifier = window.recaptchaVerifier;
			firebase
				.auth()
				.signInWithPhoneNumber("+91" + phoneNumber, appVerifier)
				.then((confirmResult) => {
					console.log(confirmResult, "LLLLLLLLLLLLLLLLL");
					// success
					window.confirmationResult = confirmResult;
					setCodeSent(true);
					setSubmitLoading(false);
					setTimer(59);
				})
				.catch((error) => {
					console.log(error, "MMMMMMMMMMMMMMM");
					// error
					window.recaptchaVerifier.render().then(function (widgetId) {
						grecaptcha.reset(widgetId);
					});
					setSubmitLoading(false);
				});
		} else {
			setSubmitLoading(true);
			if (type === "resend") {
				setSubmitLoading(false);
				const appVerifier = window.recaptchaVerifier;
				firebase
				.auth()
				.signInWithPhoneNumber("+91" + phoneNumber, appVerifier)
				.then((confirmResult) => {
					window.confirmationResult = confirmResult;
				})
				.catch((error) => {
					window.recaptchaVerifier.render().then(function (widgetId) {
						grecaptcha.reset(widgetId);
					});
				});
			}
			confirmationResult
				.confirm(code)
				.then(async (result) => {
					const user = result.user;
					var token = await firebase.auth().currentUser.getIdToken();
					dispatch(AuthActions.loginRequest(token));
					setSubmitLoading(false);
					StopTick();
				})
				.catch((error) => {
					message.error("Invalid code");
					setSubmitLoading(false);
				});
		}
	};

	const {
		error: logginError,
		loggingIn,
		logInSuccessful,
		user,
	} = useSelector((state) => state.auth);

	useEffect(() => {
		if (logginError === "Please check your username and/or password.") {
			message.error(logginError);
			dispatch(AuthActions.clearError());
		}
	}, [logginError]);

	useEffect(() => {
		dispatch(
			AuthActions.stateReset({
				logInSuccessful: false,
				logInSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		if (!loggingIn && logInSuccessful) {
			dispatch(
				AuthActions.stateReset({
					logInSuccessful: false,
				})
			);
			setIsModalVisible(false);
		}
	}, [logInSuccessful]);

	useEffect(() => {
		console.log("GGGGGGGGGGGGg", user);
	}, [user]);

	const handleCancel = () => {
		setIsModalVisible(true);
	};

	const [thanksModal, setThanksModal] = useState(false);

	return (
		<Wrapper>
			<HomeLayout>
				<Head>
					<title>TRICOTS </title>
					<meta
						name="facebook-domain-verification"
						content="qw07emt6usugfxezt4brbs0jlkxw87"
					/>
				</Head>
				<Modal
					title={
						!codeSent
							? "Enter Your Phone Number"
							: " Enter Your OTP"
					}
					visible={isModalVisible}
					onOk={handleOk}
					onCancel={handleCancel}
					closable={false}
					centered={true}
					footer={false}
				>
					{!codeSent ? (
						<Row justify="space-around" gutter={[12, 12]}>
							<Col span={4}>
								<Input value={"+91"} />
							</Col>
							<Col span={18}>
								<Input
									placeholder={"Phone number"}
									onChange={(v) =>
										setPhoneNumber(v.target.value)
									}
								/>
							</Col>
							<Col span={23}>
								<Row
									justify={"space-between"}
									gutter={[12, 12]}
								>
									<Col span={12}>
										{" "}
										<Button
											style={{ width: "100%" }}
											onClick={() => router.back()}
										>
											Cancel
										</Button>
									</Col>
									<Col span={12}>
										{" "}
										<Button
											loading={loadingOTP}
											type={"primary"}
											style={{ width: "100%" }}
											onClick={handleOk}
										>
											Submit
										</Button>
									</Col>
								</Row>
							</Col>
						</Row>
					) : (
						<Row justify="space-around" gutter={[12, 12]}>
							<Col span={23}>
								<OtpInput
									value={code}
									inputStyle={{ width: "80%" }}
									onChange={(otp) => setCode(otp)}
									numInputs={6}
									separator={<span>{``}</span>}
								/>
							</Col>

							<Col span={23}>
								<Button
									loading={loadingOTP || loggingIn}
									type={"primary"}
									style={{ width: "100%" }}
									onClick={handleOk}
								>
									Verify
								</Button>
							</Col>
							{codeSent && timer === 60 ? (
								<Col span={23}>
									<div
										style={{
											color: "blue",
											cursor: "pointer",
										}}
										onClick={() => {
											setCodeSent(true);
											message.success("Code Sent");
											setTimer(59);
											handleOk("resend");
										}}
									>
										resend code ?
									</div>
								</Col>
							) : (
								<Col span={23}>
									{codeSent && (
										<div style={{ color: "blue" }}>
											{timer + " s"}
										</div>
									)}
								</Col>
							)}
						</Row>
					)}
				</Modal>
				<Modal title="" visible={thanksModal} footer={null}>
					<Result
						status="success"
						title="Thanks for your Order"
						extra={[
							<Button
								type="primary"
								key="console"
								onClick={() => {
									router.push("/order/" + thanksModal?._id);
									setThanksModal(false);
								}}
							>
								Review Order
							</Button>,
							// <Button key="buy">Buy Again</Button>,
						]}
					/>
				</Modal>
				<div id="recaptcha-container"> </div>
				<BreadCrumb
					data={[{ name: "Home", route: "/" }, { name: "Checkout" }]}
				/>
				<div className="container">
					<Row gutter={[0, 24]}>
						<Col xs={24} lg={8}>
							<div style={{ padding: screens.xs ? 0 : 20 }}>
								<p className="checkout-headings">Your Cart</p>
								<Button
									className="back-btn"
									onClick={() => router.back()}
								>
									Continue Shopping
								</Button>

								{Object.keys(cart).map((item) => {
									return (
										<CartItem
											onClickName={(slug) =>
												router.push("/product/" + slug)
											}
											data={cart[item]}
											cart={{
												cart,
												createCartSuccessful,
												addToCartSuccessful,
												removeFromCartSuccessful,
												deleteCartSuccessful,
											}}
											onClickPlus={(
												inventory,
												quantity
											) =>
												handleClickPlus(
													inventory,
													quantity
												)
											}
											onClickMinus={(
												inventory,
												quantity
											) =>
												handleClickMinus(
													inventory,
													quantity
												)
											}
											onClickCross={(inventory) =>
												handleClickMinus(
													inventory,
													"delete"
												)
											}
										/>
									);
								})}
								{Object.keys(comboCart).map((item) => {
									return (
										<ComboCartItem
											onClickName={(slug) =>
												router.push("/combo/" + slug)
											}
											data={comboCart[item]}
											comboCart={{
												comboCart,
												addToComboCartSuccessful,
												createComboCartSuccessful,
												removeFromComboCartSuccessful,
												deleteComboCartSuccessful,
											}}
											onClickCross={(inventory) =>
												deleteComboCart(
													inventory,
													"delete"
												)
											}
										/>
									);
								})}
							</div>
						</Col>
						<Col xs={24} lg={8}>
							<div style={{ padding: screens.xs ? 0 : 20 }}>
								<p className="checkout-headings">
									Select Address
								</p>
								<Button
									className="back-btn"
									onClick={() =>
										router.push("/address/create")
									}
								>
									Create New Address
								</Button>
								{addresses?.map((address) => {
									return (
										<AddressCard
											onSelectAddress={(address) =>
												setSelectedAddress(address)
											}
											checked={
												selectedAddress?._id ===
												address._id
											}
											showRadio
											data={address}
											showActions={true}
											onClickEdit={() =>
												router.push(
													"/address/" + address._id
												)
											}
											onClickDelete={() => {}}
										/>
									);
								})}
							</div>
						</Col>

						<Col xs={24} lg={8}>
							<div style={{ padding: screens.xs ? 0 : 20 }}>
								<div style={{ marginBlock: "15px" }}>
									<p className="checkout-headings">
										Payment Mode
									</p>

									<Radio.Group
										onChange={(e) =>
											setRadio(e.target.value)
										}
									>
										<Radio value={"ONLINE"}>Online</Radio>
										{JSON.parse(
											localStorage.getItem("admin")
										)?.isCOD && (
											<Radio value={"COD"}>
												Cash On Delivery
											</Radio>
										)}
									</Radio.Group>
									<div
										style={{
											fontSize: 12,
											paddingTop: 10,
											color: Themes.primary,
										}}
									>
										Free Shipping For Prepaid Orders
									</div>
								</div>
							</div>

							<div style={{ padding: screens.xs ? 0 : 20 }}>
								<Collapse
									onChange={callback}
									style={{ marginTop: 20 }}
								>
									<Panel header="Promocodes" key="1">
										{promocodes?.map((code) => {
											return (
												<div
													style={{
														display: "flex",
														flexDirection: "row",
													}}
												>
													<TagFilled
														style={{ margin: 5 }}
													/>{" "}
													{code?.name}
												</div>
											);
										})}
									</Panel>
								</Collapse>
								{promocodes?.filter((code) => {
									if (
										totalPayable <= code.max &&
										totalPayable >= code.min &&
										(radio === code.customerType ||
											code.customerType === "BOTH")
									)
										return code;
								}).length !== 0 &&
									!appliedCode && (
										<Alert
											message={
												<div>
													{promocodes?.map((code) => {
														if (
															totalPayable <=
																code.max &&
															totalPayable >=
																code.min &&
															(radio ===
																code.customerType ||
																code.customerType ===
																	"BOTH")
														)
															return (
																<div>
																	{code?.name}
																	<Button
																		style={{
																			margin: 10,
																		}}
																		onClick={() =>
																			verifyPromocode(
																				code
																			)
																		}
																	>
																		APPLY
																		PROMOCODE
																	</Button>
																</div>
															);
													})}
												</div>
											}
											type="success"
										/>
									)}
							</div>
							<div style={{ padding: screens.xs ? 0 : 20 }}>
								<div style={{ marginBlock: "15px" }}>
									<p className="checkout-headings">
										Payments
									</p>

									{/* <Row justify="space-between">
										<Col>
											Total MRP(Inclusive Of all Taxes)
										</Col>
										<Col className="price-text">
											₹{total}
										</Col>
									</Row>
									<Row justify="space-between">
										<Col>Tricots Discount </Col>
										<Col className="price-text">
											-₹{discount}
										</Col>
									</Row> */}
									<Row justify="space-between">
										<Col>Cart Total </Col>
										<Col className="price-text">
											₹{cartTotal}
										</Col>
									</Row>
									<Row justify="space-between">
										<Col>Shipping</Col>
										<Col className="free-text">
											{shipping === 0
												? "FREE"
												: `₹${shipping}`}
										</Col>
									</Row>
									{appliedCode && (
										<Row justify="space-between">
											<Col>Promocode discount</Col>
											<Col className="free-text">
												{`₹ ${appliedCode?.deductedPrice}`}
											</Col>
										</Row>
									)}
									<Row justify="space-between">
										<Col>Total Payable </Col>
										<Col className="price-text">
											₹{Math.round(totalPayable)}
										</Col>
									</Row>
									<Row justify="center">
										<Col className="free-text">
											Congrats! You Saved ₹
											{Math.round(
												discount +
													(appliedCode?.deductedPrice
														? appliedCode?.deductedPrice
														: 0)
											)}{" "}
											On This Order
										</Col>
									</Row>
									<Row justify="center">
										{
											<Button
												loading={loading}
												onClick={() => {
													if (radio === undefined) {
														return message.error(
															"Select Payment Mode to Proceed"
														);
													} else {
														return radio ===
															"ONLINE"
															? displayRazorpay()
															: radio === "COD"
															? handlePlaceOrder()
															: handlePlaceOrder();
													}
												}}
												className="checkout-btn-web"
												type="primary"
											>
												{radio === "BOTH" ||
												radio === "COD"
													? "Place Order"
													: "Continue To Payment"}
											</Button>
										}
									</Row>
								</div>
							</div>
						</Col>
					</Row>
				</div>
				{/* {screens.xs && (
					<Button
					loading={loading}
					onClick={() => {
						if (radio === "BOTH") {
							return message.error(
								"Select Payment Mode to Proceed"
							);
						} else {
							return radio ===
								"ONLINE"
								? displayRazorpay()
								: radio === "COD"
								? handlePlaceOrder()
								: {};
						}
					}}
					className="checkout-btn"
					type="primary"
					>
						Continue To Payment
					</Button>
				)} */}
			</HomeLayout>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.ant-collapse {
		background-color: #800000;
	}
	.ant-collapse > .ant-collapse-item > .ant-collapse-header {
		color: #ffffff;
	}
	.container {
		display: flex;
		flex-direction: column;
		padding: 5px;
	}
	.checkout-btn {
		width: 100%;
		margin-top: 20px;
		position: fixed;
		bottom: 0;
		z-index: 100;
		height: 40px;
	}
	.checkout-btn-web {
		width: 100%;
		margin-top: 20px;
		z-index: 100;
		height: 40px;
	}
	.back-btn {
		border: 1px solid ${Themes.primary};
		color: ${Themes.primary};
		margin-bottom: 10px;
		width: 100%;
		height: 40px;
	}
	.checkout-headings {
		font-size: 16px;
		font-weight: 500;
		margin-top: 10px;
	}
	.price-text {
		font-size: 16px;
		font-weight: 500;
	}
	.free-text {
		/* font-size:16px; */
		font-weight: 500;
		color: ${Themes.positive};
	}
`;
