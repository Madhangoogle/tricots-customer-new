import styled from "styled-components";
import React, { useEffect, useState } from "react";
import Head from "next/head";
import { Row, Col, message, Grid } from "antd";

import Theme from "../../themes/themes";
import Layout from "../../layouts/HomeLayout";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import { BannerTop, BannerBottom } from "../../components/Banner/Banner";
import ProductCard from "../../components/ProductCard/ProductCard";
import ProductCarousal from "../../components/ProductCarousal/ProductCarousal";
import ComboPack from "../../components/ComboPack/ComboPack";
import { ShoppingCartOutlined, EyeOutlined } from "@ant-design/icons";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import generalActions from "../../redux/actions/general";
import cartActions from "../../redux/actions/comboCart";

const { useBreakpoint } = Grid;

export default function ProductView() {
	const screens = useBreakpoint();
	const dispatch = useDispatch();
	const router = useRouter();
	const [cartData, setCartData] = useState({});
	const { comboDetails, products , fetchComboDetailsSuccessful} = useSelector((state) => state.general);
	const { creatingComboCart, createComboCartSuccessful}  = useSelector(
		(state) => state.comboCart
	);

	useEffect(() => {
		dispatch(generalActions.setLoaderRequest(true));
	}, []);

	useEffect(() => {
		dispatch(
			cartActions.stateReset({
				addingToComboCart: false,
				addToComboCartSuccessful: false,

				removingFromComboCartDetails: false,
				removeFromComboCartSuccessful: false,

				creatingComboCart: false,
				createComboCartSuccessful: false,

				deletingComboCart: false,
				deleteComboCartSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		if (router.query.comboId)
			dispatch(
				generalActions.getComboDetailsRequest(router.query.comboId)
			);
	}, [router.query.comboId]);

	const handleClickProduct = (product) => {
		console.log(router.asPath.split("/").splice(1)[0] + "/" + product.slug);
		if (router.asPath.split("/").splice(1)[0])
			return router.push("/product/" + product.slug);
	};

	// const handleClickPlus = (inventory) => {
	// 	if (cartData[comboDetails._id] !== undefined) {
	// 		setCartData({
	// 			...cartData,
	// 			[comboDetails._id]: {
	// 				...cartData[comboDetails._id],
	// 				[inventory._id]: inventory,
	// 			},
	// 		});
	// 	} else {
	// 		setCartData({
	// 			...cartData,
	// 			[comboDetails._id]: {
	// 				productName: comboDetails.name,
	// 				specialPrice: comboDetails.specialPrice,
	// 				defaultPrice: comboDetails.defaultPrice,
	// 				imageUrl: comboDetails.imageId?.imageUrl,
	// 				comboId: comboDetails?._id,
	// 				comboSlug: comboDetails?.slug,
	// 				[inventory._id]: inventory,
	// 			},
	// 		});
	// 	}
	// };

	const handleClickPlus = (allSelectedInventories) => {
		if (cartData[comboDetails._id] !== undefined) {
			setCartData({
				...cartData,
				[comboDetails._id]: {
					...cartData[comboDetails._id],
					inventories: allSelectedInventories,
				},
			});
		} else {
			setCartData({
				...cartData,
				[comboDetails._id]: {
					productName: comboDetails.name,
					specialPrice: comboDetails.specialPrice,
					defaultPrice: comboDetails.defaultPrice,
					imageUrl: comboDetails.imageId?.imageUrl,
					comboId: comboDetails?._id,
					comboSlug: comboDetails?.slug,
					inventories: allSelectedInventories,
				},
			});
		}
	};

	const handleClickAddToCart = async() => {
		let noOfItemsInInventory = 0
		await Promise.all( cartData[comboDetails._id]?.inventories?.map(i => {
			noOfItemsInInventory += i.quantity
		}))
		if (
			cartData !== undefined &&
			cartData[comboDetails._id] &&
			noOfItemsInInventory ===
				comboDetails.numberOfItems
		) {
			dispatch(cartActions.createComboCartRequest(cartData));
		} else {
			message.warn(
				`Select ${comboDetails.numberOfItems} different items to proceed`
			);
		}
	};

	useEffect(() => {
		if (createComboCartSuccessful) {
			dispatch(
				cartActions.stateReset({
					createComboCartSuccessful: false,
				})
			);
			router.push("/checkout");
		}
	}, [createComboCartSuccessful]);

	useEffect(() => {
		if (!creatingComboCart && createComboCartSuccessful) {
			cartActions.stateReset({
				createComboCartSuccessful: false,
			});
		}
	}, [createComboCartSuccessful]);

	useEffect(()=> {
		if(fetchComboDetailsSuccessful){
			dispatch(generalActions.setLoaderRequest(false));
		}
	},[comboDetails])

	return (
		<Wrapper>
			<Head>
				<title>TRICOTS </title>
				<meta
					name="facebook-domain-verification"
					content="qw07emt6usugfxezt4brbs0jlkxw87"
				/>
			</Head>
			<Layout>
				{/* <BannerTop /> */}
				<BreadCrumb
					data={[
						{ name: "Home", route: "/" },
						{ name: "Combo Full Sleeve" },
					]}
				/>
				{/* <BannerBottom /> */}

				<div className="container">
					<Row gutter={[0, 12]}>
						<Col xs={24} sm={24} md={24} l={24} xl={24}>
							<ComboPack
								data={comboDetails}
								onClickPlus={handleClickPlus}
								onClickAddToCart={handleClickAddToCart}
							/>
						</Col>
					</Row>
					<Row gutter={[0, 12]}>
						{products.map((product) => {
							return (
								<Col xs={12} sm={12} md={12} l={8} xl={6}>
									<ProductCard
										data={product}
										onClick={() =>
											handleClickProduct(product)
										}
									/>
								</Col>
							);
						})}
					</Row>
				</div>
				{/* {screens.xs && Object.keys(comboCart).length !== 0 ? (
					<Button
						type={"primary"}
						icon={<EyeOutlined />}
						className="add-to-cart-btn"
						onClick={() => router.push("/cart")}
					>
						View Cart ({Object.keys(comboCart).length})
					</Button>
				) : null} */}
				{/* <div className="add-to-cart-btn"><EyeOutlined  className="icon" />View Cart (12)</div> */}
			</Layout>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.container {
		padding: 7px;
	}
	.add-to-cart-btn {
		height: 40px;
		display: flex;
		justify-content: center;
		align-items: center;
		background-color: ${Theme.primary};
		width: 100vw;
		color: ${Theme.white};
		position: fixed;
		bottom: 0;
		text-transform: uppercase;
		z-index: 100;
		margin-left: -7px;
	}
	.icon {
		margin-right: 15px;
		font-size: 22px;
		text-transform: uppercase;
	}
`;
