import styled from "styled-components";
import React, { useState, useEffect } from "react";
import Head from "next/head";
import { Row, Col, Button, Grid, message } from "antd";

import Theme from "../../themes/themes";
import Layout from "../../layouts/HomeLayout";
import BreadCrumb from "../../components/BreadCrumb/BreadCrumb";
import { BannerTop, BannerBottom } from "../../components/Banner/Banner";
import ProductCard from "../../components/ProductCard/ProductCard";
import ProductCarousal from "../../components/ProductCarousal/ProductCarousal";
import { ShoppingCartOutlined, EyeOutlined } from "@ant-design/icons";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import generalActions from "../../redux/actions/general";
import cartActions from "../../redux/actions/cart";
import WishListActions from "../../redux/actions/wishList";

const { useBreakpoint } = Grid;
export default function ProductView() {
	const screens = useBreakpoint();
	const dispatch = useDispatch();
	const router = useRouter();

	const { productDetails, products, fetchProductDetailsSuccessful } = useSelector((state) => state.general);
	const {
		cart,

		addingToCart,
		addToCartSuccessful,

		removingFromCart,
		removeFromCartSuccessful,

		creatingCart,
		createCartSuccessful,

		deletingCart,
		deleteCartSuccessful,
	} = useSelector((state) => state.cart);

	useEffect(() => {
		dispatch(
			cartActions.stateReset({
				addingToCart: false,
				addToCartSuccessful: false,

				removingFromCartDetails: false,
				removeFromCartSuccessful: false,

				creatingCart: false,
				createCartSuccessful: false,

				deletingCart: false,
				deleteCartSuccessful: false,
			})
		);
	}, []);

	useEffect(()=>{
		if(fetchProductDetailsSuccessful){
			dispatch(generalActions.setLoaderRequest(false));
		}
	},[productDetails])

	useEffect(() => {
		if (router.query.productId){
			handleLoadData()
			dispatch(generalActions.setLoaderRequest(true));
		}
			
	}, [router.query.productId]);

	const handleLoadData = ()=>{
		dispatch(
			generalActions.getProductDetailsRequest(router.query.productId)
		);
	}

	const handleClickProduct = (product) => {
		console.log(router.asPath.split("/").splice(1)[0] + "/" + product.slug);
		if (router.asPath.split("/").splice(1)[0])
			return router.push(
				"/" + router.asPath.split("/").splice(1)[0] + "/" + product.slug
			);
		router.push(product.slug);
	};

	const handleClickPlus = (inventory, quantity) => {
		if (!cart || !cart[inventory._id]) {
			dispatch(
				cartActions.createCartRequest({
					[inventory._id]: {
						...inventory,
						productName: productDetails.name,
						productImage : productDetails.imageId?.imageUrl,
						specialPrice: productDetails.specialPrice,
						defaultPrice: productDetails.defaultPrice,
						slug: productDetails.slug,
						quantity: quantity,
					},
				})
			);
		} else {
			dispatch(cartActions.addToCartRequest(inventory._id, quantity));
		}
	};

	const handleClickMinus = (inventory) => {
		if (cart && cart[inventory._id] && cart[inventory._id].quantity === 1) {
			dispatch(cartActions.deleteCartRequest(inventory._id));
		} else {
			dispatch(cartActions.removeFromCartRequest(inventory._id));
		}
	};

	useEffect(() => {
		dispatch(cartActions.clearError());
	}, []); 
	
	useEffect(() => {
		if (
			createCartSuccessful ||
			addToCartSuccessful ||
			removeFromCartSuccessful ||
			deleteCartSuccessful
		) {
			dispatch(
				cartActions.stateReset({
					addToCartSuccessful: false,
					removeFromCartSuccessful: false,
					createCartSuccessful: false,
					deleteCartSuccessful: false,
				})
			);
		}
	}, [
		createCartSuccessful,
		addToCartSuccessful,
		removeFromCartSuccessful,
		deleteCartSuccessful,
	]);

	useEffect(() => {
		if (error) {
			message.error(error);
			dispatch(WishListActions.clearError());
		}
	}, [error]);

	useEffect(() => {
		if (!creatingCart && createCartSuccessful) {
			cartActions.stateReset({
				createCartSuccessful: false,
			});
			message.success("Item added to cart successfully");
		}
	}, [createCartSuccessful]);

	useEffect(() => {
		if (!deletingCart && deleteCartSuccessful) {
			cartActions.stateReset({
				deleteCartSuccessful: false,
			});
			message.success("Cart updated successfully");
		}
	}, [deleteCartSuccessful]);

	useEffect(() => {
		if (!addingToCart && addToCartSuccessful) {
			cartActions.stateReset({
				addToCartSuccessful: false,
			});
			message.success("Item in cart updated successfully");
		}
	}, [addToCartSuccessful]);

	useEffect(() => {
		if (!removingFromCart && removeFromCartSuccessful) {
			cartActions.stateReset({
				removeFromCartSuccessful: false,
			});
			message.success("Item in a cart removed successfully");
		}
	}, [removeFromCartSuccessful]);

	const {
		error,
		wishList,
		addingToWishList,
		removingFromWishList,
		addToWishListSuccessful,
		removeFromWishListSuccessful,
	} = useSelector((state) => state.wishList);

	useEffect(() => {
		dispatch(
			WishListActions.stateReset({
				addingToWishList: false,
				removingFromWishList: false,
				addToWishListSuccessful: false,
				removeFromWishListSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		if (!addingToWishList && addToWishListSuccessful) {
			dispatch(
				WishListActions.stateReset({
					addToWishListSuccessful: false,
				})
			);
			dispatch(
				generalActions.getProductDetailsRequest(router.query.productId)
			);
		}
	}, [addToWishListSuccessful]);

	useEffect(() => {
		if (!removingFromWishList && removeFromWishListSuccessful) {
			dispatch(
				WishListActions.stateReset({
					removeFromWishListSuccessful: false,
				})
			);
			dispatch(
				generalActions.getProductDetailsRequest(router.query.productId)
			);
		}
	}, [removeFromWishListSuccessful]);

	const addToWishList = (productId) => {
		dispatch(WishListActions.addToWishListRequest(productId));
	};

	const removeFromWishList = (productId) => {
		dispatch(WishListActions.removeFromWishListRequest(productId));
	};

	return (
		<Wrapper>
			<Head>
				<title>TRICOTS </title>
				<meta name="facebook-domain-verification" content="qw07emt6usugfxezt4brbs0jlkxw87" />
			</Head>
			<Layout>
				<BreadCrumb
					data={[
						{ name: "Home", route: "/" },
						{ name: productDetails?.name },
					]}
				/>

				<div className="container">
					<Col xs={24} sm={24} md={24} l={24} xl={24}>
						<ProductCarousal
							addToWishList={(productId) =>
								addToWishList(productId)
							}
							removeFromWishList={(productId) =>
								removeFromWishList(productId)
							}
							cart={{
								cart,
								createCartSuccessful,
								addToCartSuccessful,
								removeFromCartSuccessful,
								deleteCartSuccessful,
							}}
							data={productDetails}
							onClickPlus={(inventory, quantity) =>
								handleClickPlus(inventory, quantity)
							}
							onClickMinus={(inventory) =>
								handleClickMinus(inventory)
							}
							onClickViewCart={() => router.push("/checkout")}
							hanldeRefresh={handleLoadData}
						/>
					</Col>
					<Row gutter={[0, 12]}>
						{products.map((product) => {
							return (
								<Col xs={12} sm={12} md={12} l={8} xl={6}>
									<ProductCard
										addToWishList={(productId) =>
											addToWishList(productId)
										}
										removeFromWishList={(productId) =>
											removeFromWishList(productId)
										}
										data={product}
										onClick={() =>
											handleClickProduct(product)
										}
									/>
								</Col>
							);
						})}
					</Row>
				</div>
				{/* {screens.xs && (
					<>
						<div className="add-to-cart-btn">
							<ShoppingCartOutlined className="icon" />
							Add To Cart
						</div>
						<div className="add-to-cart-btn">
							<EyeOutlined className="icon" />
							View Cart (12)
						</div>
					</>
				)} */}
			</Layout>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.container {
		padding: 7px;
	}
	.add-to-cart-btn {
		/* padding: 10px; */
		height: 40px;
		display: flex;
		justify-content: center;
		align-items: center;
		background-color: ${Theme.primary};
		width: 100vw;
		color: ${Theme.white};
		position: fixed;
		bottom: 0;
		text-transform: uppercase;
		z-index: 100;
		margin-left: -7px;
	}
	.icon {
		margin-right: 15px;
		font-size: 22px;
		text-transform: uppercase;
	}
`;
