import styled from "styled-components";
import React, { useEffect, useState } from "react";
import Head from "next/head";
import { Row, Col, message } from "antd";

import Themes from "../../themes/themes";
import Layout from "../../layouts/HomeLayout";
import BreadCrumb from "../../components/BreadCrumb";
import { BannerTop, BannerBottom } from "../../components/Banner";
import ProductCard from "../../components/ProductCard";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import generalActions from "../../redux/actions/general";
import WishListActions from "../../redux/actions/wishList";
import axios from "axios";
import { API_URL } from "../../constants";

export default function Signup() {
	const dispatch = useDispatch();
	const router = useRouter();
	const { products } = useSelector((state) => state.general);
	const [collection, setColection] = useState([]);

	useEffect(() => {
		if (router.query.collectionId) {
			dispatch(
				generalActions.getProductsRequest(router.query.collectionId)
			);
			dispatch(generalActions.setLoaderRequest(true));
			getData();
		}
	}, [router.query.collectionId]);

	const getData = () => {
		axios
			.get(API_URL + "/guest/collections/" + router.query.collectionId, {
				headers: {
					Authorization: "JWT " + localStorage.getItem("accessToken"),
				},
			})
			.then((res) => {
				setColection(res.data.data);
				dispatch(generalActions.setLoaderRequest(false));
			});
	};

	const handleClickProduct = (product) => {
		router.push(router.query.collectionId + "/" + product.slug);
	};

	const {
		error,
		wishList,
		addingToWishList,
		removingFromWishList,
		addToWishListSuccessful,
		removeFromWishListSuccessful,
	} = useSelector((state) => state.wishList);

	useEffect(() => {
		dispatch(
			WishListActions.stateReset({
				addingToWishList: false,
				removingFromWishList: false,
				addToWishListSuccessful: false,
				removeFromWishListSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		if (error) {
			message.error(error);
			dispatch(WishListActions.clearError());
		}
	}, [error]);

	useEffect(() => {
		if (!addingToWishList && addToWishListSuccessful) {
			dispatch(
				WishListActions.stateReset({
					addToWishListSuccessful: false,
				})
			);
			dispatch(
				generalActions.getProductsRequest(router.query.collectionId)
			);
		}
	}, [addToWishListSuccessful]);
	useEffect(() => {
		if (!removingFromWishList && removeFromWishListSuccessful) {
			dispatch(
				WishListActions.stateReset({
					removeFromWishListSuccessful: false,
				})
			);
			dispatch(
				generalActions.getProductsRequest(router.query.collectionId)
			);
		}
	}, [removeFromWishListSuccessful]);

	const addToWishList = (productId) => {
		dispatch(WishListActions.addToWishListRequest(productId));
	};

	const removeFromWishList = (productId) => {
		dispatch(WishListActions.removeFromWishListRequest(productId));
	};
	return (
		<Wrapper>
			<Head>
				<title>TRICOTS </title>
				<meta
					name="facebook-domain-verification"
					content="qw07emt6usugfxezt4brbs0jlkxw87"
				/>
			</Head>
			<Layout>
				<BannerTop data={[]} />
				<BreadCrumb
					data={[
						{ name: "Home", route: "/" },
						{ name: collection.collectionName },
					]}
				/>
				<BannerBottom
					data={[
						{
							imageId: { imageUrl: collection.bannerUrl },
							mobileImageId: {
								imageUrl: collection.mobileBannerUrl,
							},
						},
					]}
				/>
				<div
				style={{paddingInline:"10%", paddingBlock:"2%"}}
					dangerouslySetInnerHTML={{
						__html: collection?.collectionDescription,
					}}
				></div>
				<div className="container">
					<Row gutter={[0, 12]}>
						{products.map((product) => {
							return (
								<Col xs={12} sm={12} md={12} l={8} xl={6}>
									<ProductCard
										addToWishList={(productId) =>
											addToWishList(productId)
										}
										removeFromWishList={(productId) =>
											removeFromWishList(productId)
										}
										data={product}
										onClick={() =>
											handleClickProduct(product)
										}
									/>
								</Col>
							);
						})}
					</Row>
				</div>
			</Layout>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.container {
		padding: 7px;
	}
`;
