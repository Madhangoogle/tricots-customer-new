import styled from "styled-components";
import Themes from "../themes/themes";
import React, { useEffect, useState } from "react";
import { message, Grid, Row, Col } from "antd";
import Head from "next/head";
import { DownOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";

import Header from "../components/Header";
import Footer from "../components/Footer";
import ProductCard from "../components/ProductCard/ProductCard";
import Categories from "../components/Categories";
import Collections from "../components/Collections";
import { BannerTop, BannerBottom } from "../components/Banner";
import GeneralActions from "../redux/actions/general";
import Layout from "../layouts/HomeLayout";

const { useBreakpoint } = Grid;

export default function Home() {
	const router = useRouter();
	const dispatch = useDispatch();
	const screens = useBreakpoint();

	const {
		error,
		fetchingBanners,
		fetchBannersSuccessful,
		banners,
		fetchingCollections,
		fetchCollectionsSuccessful,
		collections,
	} = useSelector((state) => state.general);

	useEffect(() => {
		dispatch(GeneralActions.setLoaderRequest(true))
		dispatch(GeneralActions.clearError());
	}, []);

	useEffect(() => {
		dispatch(
			GeneralActions.stateReset({
				fetchingBanners: false,
				fetchBannersSuccessful: false,
				fetchingCollections: false,
				fetchCollectionsSuccessful: false,
				fetchingProducts: false,
				fetchProductsSuccessful: false,
			})
		);
	}, []);

	useEffect(() => {
		if (error) {
			message.error(error);
		}
	}, [error]);

	useEffect(() => {
		dispatch(GeneralActions.getBannersRequest());
		dispatch(GeneralActions.getCollectionsRequest());
		// dispatch(GeneralActions.getProductsRequest("bestSale"))
		// dispatch(GeneralActions.getProductsRequest("isFeatured"))
	}, []);

	useEffect(() => {
		if (!fetchingBanners && fetchBannersSuccessful) {
			dispatch(GeneralActions.setLoaderRequest(false));
			dispatch(
				GeneralActions.stateReset({
					fetchBannersSuccessful: false,
				})
			);
		}
	}, [fetchBannersSuccessful]);

	useEffect(() => {
		if (!fetchingCollections && fetchCollectionsSuccessful) {
			dispatch(
				GeneralActions.stateReset({
					fetchCollectionsSuccessful: false,
				})
			);
		}
	}, [fetchCollectionsSuccessful]);

	// useEffect(() => {
	// 	if (!fetchingProducts && fetchProductsSuccessful) {
	// 		dispatch(
	// 			GeneralActions.stateReset({
	// 				fetchProductsSuccessful: false,
	// 			})
	// 		);
	// 	}
	// }, [fetchProductsSuccessful]);

	return (
		<Wrapper size={screens.xs ? "mobile" : "web"}>
			<Head>
				<title>TRICOTS </title>
				{/* <meta name="facebook-domain-verification" content="qw07emt6usugfxezt4brbs0jlkxw87" /> */}
			</Head>
			<Layout>
			<div className="main-container">
				<BannerBottom
					data={banners?.filter((banner) => {
						if (banner.type === "large") return banner;
					})}
				/>

				<Collections
					data={collections}
					onClickCollection={(route) => router.push(route)}
				/>
		
			
			</div>
			</Layout>
		</Wrapper>
	);
}

const Wrapper = styled.section`
	font-family: "Montserrat", sans-serif;
	width: 100vw;
	width: 100%;
	.main-container {
		padding: 7px;
	}
	.happy-customer {
		font-size: ${(props) => (props.size === "mobile" ? "18px" : "35px")};
		display: flex;
		justify-content: center;
		margin-block: 20px;
	}
	.customer-count {
		font-size: ${(props) => (props.size === "mobile" ? "18px" : "35px")};
		display: flex;
		justify-content: center;
		font-weight: bold;
		margin-inline: ${(props) => (props.size === "mobile" ? "5px" : "20px")};
		color :#800000
	}
	.indian-brand {
		font-size: ${(props) => (props.size === "mobile" ? "15px" : "35px")};
		display: flex;
		justify-content: center;
		margin-block: 20px;
		background-color:#800000;
		color: #fff;
		padding: ${(props) => (props.size === "mobile" ? "10px" : "0px")};
	}
`;
